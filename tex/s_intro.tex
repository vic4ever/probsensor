\section{Introduction}
\label{sec:intro}

Perhaps, one of the most effective ways to deal with imprecise and uncertain data is to employ probabilistic approaches. Once probability values are given, such that each probability value indicates the certainty of a given data value, we can infer more reliable responses with respect to a variety of queries~\cite{cheng03,Hua2008}.
This has led to a new paradigm in database research, which considers effective and efficient techniques for managing uncertain data based on \emph{probabilistic databases}~\cite{Cavallo1987,Dalvi2007a}.

Existing work on data processing in probabilistic databases is based on the assumption that prior knowledge on probability values and distributions exists; however, this is not true. In fact, \emph{creating probabilistic databases} is a challenging and unresolved problem, whereas a variety of tools for processing probabilistic databases already exist. Prior work on this problem has only limited scope for domain-specific applications, such as handling duplicated tuples~\cite{Andritsos2006,Hassanzadeh2009} and deriving structured data from unstructured data~\cite{Gupta2006}. A wide range of applications still lack the benefits of existing techniques for data processing in probabilistic databases.
%
Time-series data is one important example where probabilistic data processing is currently not widely applicable due to the lack of probability values, although the benefits are evident, given that time series are often imprecise and uncertain.
%For example, applications in sensor networks commonly perform aggregate queries over time series of imprecise sensor readings.
This data uncertainty originates from various sources, such as low-cost sensors, discharged batteries, and network failures.
%Time-stamped GPS logs generally contain positional errors, yet query processing on this type of data typically neglects the errors. All these problems, however, can be effectively covered by the concept of probabilistic databases (or streams) and its relevant techniques, which are already well-established~\cite{Cormode2007,re08,Letchner2009}.
Such uncertainty of time series, however, can be effectively dealt with by using concepts from probabilistic databases \cite{Cormode2007,re08,Letchner2009} once the probability values are provided.

%The only requirement for this is to have a method for populating imprecise time series with appropriate probability values that reflect how reliable each value in the time series is.

Unfortunately, creating probabilistic databases from imprecise time-series data is a difficult problem. Time series are likely to exhibit highly irregular dependencies on time. For example, temperature changes dramatically around sunrise and sunset, but changes only slightly during night.
%GPS errors may increase or decrease according to the presence of obstacles (e.g., tunnels and high buildings), while moving.
This implies that the probability distributions that are used as the basis for deriving probability values also change over time and thus must be computed dynamically.
%, while facilitating efficient computation for a wide class of real-time applications on time series.
%
Addressing this challenge, we propose novel techniques for creating tuple-level probabilistic databases (or data streams) from imprecise time series. To the best of our knowledge, this is the first work that offers generic solutions for creating probabilistic databases from arbitrary imprecise time series. Some related work~\cite{Khoussainova2006,Tran2009} on this problem is applicable only for specific data types (i.e., RFID data).
%We also aim at generating probability databases in an automated manner; whenever a new value is streamed to the system, its associated probability value is populated instantly.

The core idea of our approach is to dynamically infer probability distributions of imprecise raw values from previous values of the time series. We then employ the inferred probability distributions for forming a probabilistic database. \figref{fig:fid2prob} illustrates an example of creating a probability database stream from a normal distribution $p(R)$, where $R$ is the random variable associated with Bob's position.
%
\begin{figure}[b]
    \center
    \includegraphics[width=1.0\columnwidth]{fig/fid2prob}
    \caption{An example of creating a tuple-level probabilistic database from time-dependent probability distributions.}
    \label{fig:fid2prob}
\end{figure}
%
Given $p(R)$, the probability that Bob exists at a specific room is computed by the integral of the intersection between each room and $p(R)$, which forms a tuple-level probability in the database. In a similar manner, we can derive various types of other probabilistic databases from probability distributions.
%, such as a database containing probabilities on which road a moving object resides.

%In addition to creating probabilistic databases, inferring a probability distribution can be useful for applications dealing with data quality over imprecise time-series data. The probability of a raw value in the inferred probability distribution reflects the degree of precision for the raw value. Therefore, this may be used for cleaning dirty or noisy data whose probabilities are low in the inferred distribution. If such low probabilities are found over many consecutive raw values, it may mean a sensor or network failed.

This paper introduces several {\em dynamic density metrics} that infer time-dependent probability distributions from imprecise time-series data. These probability distribution are then used for creating probabilistic databases.
%as well as the quality-aware processing of imprecise time-series data.
%The dynamic density metrics are built upon various mathematical models.
%
%They first infer expected true values for imprecise raw values in time series, using either the \arma (AutoRegressive Moving Average) model~\cite{shumway2005} or Kalman filters~\cite{shumway2005}. During this process, the metrics compute conditional variances for estimating volatilities of the time series, where the variances are then used for deriving time-varying probability distributions.
%
Our main proposal among these metrics adopts the GARCH~(Generalized AutoRegressive Conditional Heteroskedasticity) model~\cite{shumway2005} that has been widely used for forecasting volatility of time series. Here, we show that the GARCH model can also play an important role in creating probabilistic databases, by effectively inferring dynamic probability distributions.

Furthermore, we extend the GARCH metric to be robust against erroneous values. The GARCH model has been generally used over precise, certain, and clean data (e.g., stock market data). In contrast, time series that this study considers are typically imprecise and erroneous. When erroneous values are used by the GARCH model, the performance of the GARCH model decreases significantly. To cope with this, we propose an improved version of the GARCH model, termed the C-GARCH model, that performs appropriately regardless of the presence of such erroneous values.

We also introduce the {\em $\Omega$--View builder} that creates probabilistic databases based on the probability distributions computed by the dynamic density metrics. The creation of probabilistic data, however, often requires computationally expensive numerical evaluations of complex integrals. To boost the efficiency of this processing, we propose the {\em $\sigma$--cache} that reuses the information derived from probability values at previous times. This permits the $\Omega$--View builder to minimize the cost of execution of numerical evaluations.

In summary, the main contributions of this paper encompass:

\begin{itemize}

\item \setlength{\parskip}{-2pt}
  {\bf Foundation:} we introduce the concept of creating probabilistic databases from imprecise time-series data. This includes queries, framework, and quantifying the quality of dynamic density metrics (\secref{sec:prob}).

\item \setlength{\parskip}{-2pt}
 {\bf Dynamic density metrics:} we first present two naive metrics that infer time-varying probability distributions for data values in time series (\secref{sec:metrics}). We then propose more effective metrics based on the \garch~model
 % and Kalman filters
 (\secref{sec:garch}).

\item \setlength{\parskip}{-2pt}
 {\bf \ourgarch~model:} we propose an enhanced GARCH model that filters out outliers through preprocessing steps, before the outliers are used for the GARCH model. This extension is substantially robust against erroneous data  (\secref{sec:xgarch}).
 %We also provide in-depth comparative analysis among all metrics.

\item \setlength{\parskip}{-2pt}
 {\bf $\Omega$--View builder:} we introduce an efficient mechanism for the creation of probabilistic databases, built upon our $\sigma$--cache that reuses the information of prior processing for current database creation (\secref{sec:query}).

\item \setlength{\parskip}{-2pt}
 {\bf Experiments on real data:} we provide comprehensive experimental evaluations over two real datasets
 %, which compares the advantages and disadvantages of each dynamic density metric.
 (\secref{sec:exp}).

\end{itemize} 
