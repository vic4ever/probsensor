\section{Probabilistic View Generation}\label{sec:query}

Recall \defref{def:prob-esti-query} that defines the query for generating probability values for a tuple-independent probabilistic database
(view). To precisely specify the user-defined range $\Omega$ in the definition, we define $\Omega= \{\hat{r}_t + \lambda\Delta | \lambda =
-\frac{n}{2},\ldots, \frac{n}{2}\}$, where $\Delta$ is a positive real number and $n$ is an even integer. We refer to $\Delta$ and
$n$ as {\em view parameters}. These parameters describe $n$ ranges of size $\Delta$ around the expected true value $\hat{r}_t$.
In the online mode of our system, the query is evaluated at each time when a new value is streamed to the system. In the offline
mode, all necessary parameters can be specified by users using a SQL-like syntax. For example, the syntax in
\figref{fig:pv-query} creates the probabilistic view in \figref{fig:framework}.
%

\begin{figure}[!h]
\centering
\begin{tabular}{l}
  \hline\hline
  \texttt{CREATE VIEW prob\_view AS DENSITY r}\\
  \texttt{OVER t OMEGA delta=2, n=2}\\
  \texttt{FROM raw\_values WHERE t >= 1 AND t <= 3}\\
  \hline
\end{tabular}
\caption{Example of the probabilistic view generation query.}
\label{fig:pv-query}
\end{figure}

In the example shown in \figref{fig:pv-query}, \texttt{AS DENSITY r OVER t} illustrates the time-varying density for time series
\texttt{r}. The \texttt{OMEGA} clause specifies the ranges of the data values of the probabilistic view, and the \texttt{WHERE}
clause defines a time interval. Notice that the query given in \defref{def:prob-esti-query} is evaluated at each time $t$ to obtain
$\Lambda_t$. Specifically, at each $t$ and for each $\lambda = \{-\frac{n}{2}, \ldots, (\frac{n}{2}-1)\}$
we compute the following integral:

\begin{align}
\label{eqn:rho-lambda}
\rho_\lambda &= \int_{\hat{r}_t + \lambda\Delta}^{\hat{r}_t + (\lambda+1)\Delta} p_t(R_t) dR_t,  \nonumber \\
         &= P_t(R_t = \hat{r}_t + (\lambda+1)\Delta) - P_t(R_t = \hat{r}_t + \lambda\Delta),
\end{align}
where $P_t(R_t)$ is the cumulative distribution function of $r_t$. 

In short, \eqnref{eqn:rho-lambda} involves computing $P_t(R_t)$ for each value of $\lambda = \{-\frac{n}{2}, \ldots,
\frac{n}{2}\}$. Unfortunately, this computation may incur high cost when the time interval specified by the query spans
over many days comprising of a large number of raw values. Moreover, this processing becomes significantly challenging when the
query requests for a view with finer granularity (low $\Delta$) and large range $n$, since such values for the view parameters
considerably increase the computational cost.

To address this problem, we propose an approach that caches and reuses the computations of $P_t(R_t)$, which were already
performed at earlier times. The intuition behind this approach is to observe that probability distributions for a time series do
not generally exhibit dramatic changes in short terms. For example, temperature values often exhibit only slight changes within
short time intervals. In addition, similar probability distributions may be found periodically (e.g., early morning hours every
day). Thus, the query processing can take advantage of the results from previous computation. In the rest of this section, we
introduce an effective caching mechanism, termed {\em $\sigma$--cache}, that substantially boosts the performance of query
evaluation by caching the values of $P_t(R_t)$.

%Moreover, given values of $\Delta$ and $n$ we only need to compute the values of $P_t(R_t = \hat{\mu}_t + \Delta(\lambda+1))$ and $P_t(R_t = \hat{\mu}_t + \Delta\lambda)$.

%\begin{figure}[h!]
%    \center
%    \includegraphics[width=1.0\columnwidth]{gp-fig/create-pdb-query-ex}
%    \caption{Example of creating a probabilistic view using $\sigma$--cache.}
%    \label{fig:create-pdb}
%\end{figure}

\subsection{{\Large $\sigma$}--cache}
\label{sec:sigma-cache}

As introduced before, let $P_t(R_t)$ be a Gaussian cumulative distribution function of $r_t$ at time $t$. If required for clarity,
we denote it as $P_t(R_t;\hat{\Theta}_t)$ where $\hat{\Theta}_t = (\hat{r}_t,\hat{\sigma}^2_t)$. Observe that the shape of
$P_t(R_t;\hat{\Theta}_t)$ is completely determined by $\hat{\sigma}^2_t$, since $\hat{r}_t$ only specifies the location of the
curve traced by $P_t(R_t;\hat{\Theta}_t)$. This observation leads to an important property: suppose we move from time $t$ to $t'$,
then the values of $P_t(R_t = \hat{r}_t +\lambda\Delta;\hat{\Theta}_t)$, $P_{t'}(R_{t'}
=\hat{r}_{t'}+\lambda\Delta;\hat{\Theta}_{t'})$, and consequently $\rho_\lambda$ are the same if $\hat{\sigma}_t$ is equal to
$\hat{\sigma}_{t'}$. We illustrate this property graphically in \figref{fig:sigma-ind}. Moreover, since the shapes of
$P_t(R_t;\hat{\Theta}_t)$ and $P_{t'}(R_{t'};\hat{\Theta}_{t'})$ solely depend on $\hat{\sigma}_t$ and $\hat{\sigma}_{t'}$
respectively, we can assume in the rest of the analysis that the mean values of $P_t(R_t)$ and $P_{t'}(R_{t'})$ are zero. This
could be done using a simple mean shift operation on $P_t(R_t)$ and $P_{t'}(R_{t'})$.

Our aim is to approximate $P_{t'}(R_{t'})$ with $P_{t}(R_{t})$. This is possible only if we know how the distance (similarity)
between $P_t(R_t;\hat{\Theta}_t)$ and $P_{t'}(R_{t'};\hat{\Theta}_{t'})$ behaves as a function of $\hat{\sigma}_t$ and
$\hat{\sigma}_{t'}$. If we know this relation then we can, with a certain error, approximate $P_{t'}(R_{t'};\hat{\Theta}_{t'})$
with $P_t(R_t;\hat{\Theta}_t)$ simply by looking up $\hat{\sigma}_t$ and $\hat{\sigma}_{t'}$. Thus, if we have already computed
$P_t(R_t;\hat{\Theta}_t)$ at time $t$ then we can reuse it at time $t'$ to approximate
$P_{t'}(R_{t'};\hat{\Theta}_{t'})$. 

\begin{figure}[h!]
    \center
    \includegraphics[width=0.95\columnwidth]{fig/sigma-index-const}
    \caption{An example illustrating that $\rho_\lambda$ remains unchanged under mean shift operations when two Gaussian distributions have equal variance.}
    \label{fig:sigma-ind}
\end{figure}

\subsection{Constraint-Aware Caching}
In practice, systems that use the $\sigma$--cache could have constraints of limited storage size or of error tolerance. To reflect this,
 we guarantee certain user-defined constraints. Specifically, we focus on the following: 
\begin{itemize}
\item \setlength{\parskip}{-1pt} \emph{Distance constraint} guarantees that the maximum approximation error is upper bounded by the distance constraint when
  the cache is used.
\item \setlength{\parskip}{-1pt} \emph{Memory constraint} guarantees that the cache does not use more memory than that specified by the memory
constraint.
\end{itemize}

Before proceeding further, we first characterize the distance between two probability distributions using a measure known as the
\emph{Hellinger distance} \cite{pollard:2002}. It is a distance measure similar to the popular Kullback-–Leibler
divergence. However, unlike the Kullback-–Leibler divergence, the Hellinger distance takes values between zero and one which makes
its choice simple and intuitive. Formally, the square of Hellinger distance $\mathcal{H}$ between $P_t(R_{t})$ and
$P_{t'}(R_{t'})$ is given as:
%
\begin{equation}
\label{eqn:hdist}
\mathcal{H}^2[P_t(R_{t}),P_{t'}(R_{t'})] = 1-\sqrt{\frac{2 \hat{\sigma}_t \hat{\sigma}_{t'}}{\hat{\sigma}^2_t +\hat{\sigma}^2_{t'}}}.
\end{equation}
% 
The Hellinger distance assigns minimum value of zero when $P_{t'}(R_{t'})$ and $P_t(R_t)$ are the same and vice versa. 

\stitle{Guaranteeing Distance Constraint}
We use the
Hellinger distance to prove the following theorem that allows us to approximate $P_{t'}(R_{t'})$ with $P_t(R_{t})$.
%
\begin{theorem}
\label{thm:error-gar}
Given $P_{t'}(R_{t'})$, $P_t(R_t)$, and a user-defined distance constraint $\mathcal{H}'$, we can approximate $P_{t'}(R_{t'})$ with $P_t(R_t)$, such that
$\mathcal{H}[P_t(R_{t}),P_{t'}(R_{t'})] \leq \mathcal{H}'$, where $\hat{\sigma}_{t'}= d_s \cdot \hat{\sigma}_t$ and $\hat{\sigma}_{t'} > \hat{\sigma}_t$. The parameter
$d_s$ can be chosen as any value satisfying,
\begin{equation}
\label{eqn:error-gar}
d_s \leq \frac{2+\sqrt{4-4 \left(1-\mathcal{H'}^2\right)^4}}{2 \left(1-\mathcal{H'}^2\right)^2}.
\end{equation}
\end{theorem}
%
\begin{proof} Substituting $\hat{\sigma}_{t'} = d_s \cdot \hat{\sigma}_{t}$ in \eqnref{eqn:hdist} we obtain,
\begin{displaymath}
(1-\mathcal{H}'^2)\sqrt{1+d_s^2}-\sqrt{2\cdot d_s} = 0.
\end{displaymath}
Solving for $d_s$ we obtain,
\begin{displaymath}
d_s \leq \frac{2+\sqrt{4-4 \left(1-\mathcal{H'}^2\right)^4}}{2 \left(1-\mathcal{H'}^2\right)^2}.
\end{displaymath}
Since $d_s$ is monotonically increasing in $\mathcal{H}'$, choosing a value of $d_s$ as given by the above inequality guarantees the distance constraint $\mathcal{H}'$.
\end{proof}
% 

The above theorem states that if we have a user-defined \emph{distance constraint} $\mathcal{H}'$ then we can approximate
$P_{t'}(R_{t'})$ by $P_t(R_{t})$ only if $ \hat{\sigma}_{t'} > \hat{\sigma}_{t}$ and $d_s$ is chosen using
\eqnref{eqn:error-gar}. Moreover, since $d_s$ is defined as the ratio between $\hat{\sigma}_{t'}$ and $\hat{\sigma}_{t}$ we call it the
\emph{ratio threshold}.

\begin{figure}[h!]
    \center
    \includegraphics[width=0.95\columnwidth]{fig/sigma-cache}
    \caption{Structure of the $\sigma$--cache.}
    \label{fig:sigma-cache}
\end{figure}

Now, we describe how \thmref{thm:error-gar} allows us to efficiently store and reuse values of $P_t(R_t)$ while query processing.
First, we compute the maximum and minimum values amongst all $\hat{\sigma}_t$ matching the \texttt{WHERE} clause of the probabilistic
view generation query (see \figref{fig:pv-query}). Let us denote these extremes as $max(\hat{\sigma}_t)$ and
$min(\hat{\sigma}_t)$. We then define the \emph{maximum ratio threshold} $\mathcal{D}_s$ as,
%
\begin{equation}
\label{eqn:max-rt}
\mathcal{D}_s = \frac{max(\hat{\sigma}_t)}{min(\hat{\sigma}_t)}.
\end{equation}
%
Given the user-defined distance constraint $\mathcal{H}'$ we use \eqnref{eqn:error-gar} to obtain a suitable value for $d_s$. Then we compute a
$\mathcal{Q}$, such that,
\begin{equation}
\label{eqn:sigma-step}
max(\hat{\sigma}_t) = d_s^{\mathcal{Q}}\cdot min(\hat{\sigma}_t).
\end{equation}
% 
Let $\lceil x \rceil$ denote the smallest integer value that is not smaller than $x$. Then, $\lceil \mathcal{Q} \rceil$ gives us the maximum number of distributions that we should cache such that the distance constraint
is satisfied. We populate the cache by pre-computing values for $\lceil \mathcal{Q} \rceil$ distributions having standard deviations 
$d_s^q\cdot min(\hat{\sigma}_t)$, where $q=1,2,\ldots,\lceil\mathcal{Q} \rceil$. As shown in \figref{fig:sigma-cache}, these values are
computed at points specified by the view parameters $\Delta$ and \nolinebreak $n$.  

We store each of these pre-computed distributions in a
sorted container like a B-tree along with key $d_s^q \cdot min(\hat{\sigma}_t)$. When we need to compute $P_{t'}(R_{t'};\hat{\Theta}_{t'})$, we
first look up the container to find keys $d_s^q \cdot min(\hat{\sigma}_t)$ and $d_s^{q+1} \cdot min(\hat{\sigma}_t)$, such that $\hat{\sigma}_{t'}$ lies between them. We
then use the values associated with key $d_s^q \cdot min(\hat{\sigma}_t)$ for approximating
$P_{t'}(R_{t'})$. By following this procedure we always guarantee that the distance constraint is satisfied due to
\thmref{thm:error-gar}. 
%Note that we can also store $\Lambda_t$ (see \eqnref{eqn:rho-lambda}) instead of $P_t(R_t)$ without affecting the theoretical analysis.


\stitle{Guaranteeing Memory Constraint} 
%The next type of constraint restricts the number of distributions $\mathcal{Q}$ that we
%can cache. $\mathcal{Q}$ can then indicate the memory constraint since our framework assigns a constant memory size for each distribution. 
Let us assume that we have a user-defined memory constraint $\mathcal{M}$. We then consider an integer $\mathcal{Q}'$ which indicates the maximum
number of distributions that can be stored in the memory size $\mathcal{M}$. Here we prove an important theorem that enables the guarantee for memory constraint. 
\begin{theorem}
\label{thm:mem-gar}
Given the values of $\mathcal{Q}'$, $max(\hat{\sigma}_t)$, and $min(\hat{\sigma}_t)$, the memory constraint $\mathcal{M}$ is satisfied if and only if the value of the ratio threshold $d_s$ is chosen as,  
\begin{equation}
\label{eqn:mem-gar}
d_s \geq \mathcal{D}_s^{\frac{1}{\mathcal{Q}'}}. %max(\hat{\sigma}_t)^{\frac{1}{\mathcal{Q}'}} \cdot min(\hat{\sigma}_t)^{-\frac{1}{\mathcal{Q}'}}.
\end{equation}
\end{theorem}
%
\begin{proof} From \eqnref{eqn:sigma-step} we obtain,
\begin{align}
\log_e(max(\hat{\sigma}_t)) & = \mathcal{Q}'\cdot \log_e(d_s) + \log_e(min(\hat{\sigma}_t)), \nonumber \\ 
%\log_e(d_s) & = \frac{1}{\mathcal{Q}'}\log_e\left( \frac{max(\hat{\sigma}_t)}{min(\hat{\sigma}_t}\right) \nonumber\\
d_s & = max(\hat{\sigma}_t)^{\frac{1}{\mathcal{Q}'}} \cdot min(\hat{\sigma}_t)^{-\frac{1}{\mathcal{Q}'}}. \nonumber
\end{align}
From the above equation we can see that $d_s$ is monotonically decreasing in $\mathcal{Q}'$. Since $\mathcal{D}_s = \frac{max(\hat{\sigma}_t)}{min(\hat{\sigma}_t)}$, we obtain,
\begin{displaymath}
d_s \geq \mathcal{D}_s^{\frac{1}{\mathcal{Q}'}}.
\end{displaymath}
Choosing a value for $d_s$ as given in the above equation guarantees that at most $\mathcal{Q}'$ distributions are stored, thus guaranteeing the memory constraint $\mathcal{M}$.
\end{proof}
%

The above theorem states that given user-defined memory constraint $\mathcal{Q}'$ we set $d_s$ according to \eqnref{eqn:mem-gar}
so as not to store more than $\mathcal{Q}'$ distributions. Also, given a distance constraint $\mathcal{H}'$ the rate at which the
memory requirement grows is $\mathcal{O}(log(\mathcal{D}_s))$. Thus the cache size does not depend on the \emph{number} of tuples
that match the \texttt{WHERE} clause of the query in \figref{fig:pv-query}. Instead, it only grows logarithmically with the
ratio between $max(\hat{\sigma}_t)$ and $min(\hat{\sigma}_t)$. Observe that the number of distributions stored by the
$\sigma$--cache is independent from the view parameters $\Delta$ and $n$. This is a desirable property since it implies that,
queries with finer granularity are answered by storing the same number of distributions.

There is an interesting trade-off between the distance constraint and the memory constraint (see \eqnref{eqn:error-gar}
and \eqnref{eqn:mem-gar}). When the distance constraint increases, the amount of memory required by the $\sigma$--cache decreases in order to guarantee the
distance constraint and vice versa. Thus, as expected, there exists a give-and-take relationship between available memory size and prescribed error tolerance.
%Thus, we not only obtained guarantees which would allow us to choose the ratio threshold
%$d_s$ under memory and distance constraints but we showed that there exists a trade-off between these constraints and one should be
%scarified to gain the other. 

In the following section, we will demonstrate significant improvement with respect to query processing by using the $\sigma$--cache. 


%% \begin{figure}[h!]
%%     \center
%%     \includegraphics[width=0.9\columnwidth]{fig/hdist-tradeoff}
%%     \caption{Example illustrating that $\rho_\lambda$ remains unchanged under mean shifts when two Gaussian distributions have equal variance.}
%%     \label{fig:sigma-ind}
%% \end{figure}
