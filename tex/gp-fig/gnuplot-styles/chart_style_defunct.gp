LINWID = 1.5
PNTSIZ = 1.25

STY1 = "lp lt 1 lw LINWID ps PNTSIZ pt 10"
STY2 = "lp lt 1 lw LINWID ps PNTSIZ pt 2"
STY3 = "lp lt 1 lw LINWID ps PNTSIZ pt 3"
STY4 = "lp lt 1 lw LINWID ps PNTSIZ pt 4"

# Impulses styles
IMP_STY1 = "impulses lt 1 lw 50"

# Bar plot styles
BOX_STY1 = "boxes fs solid"
BOX_STY2 = "boxes fs solid 0.25"

## Set font size and offset for labels
set ylabel font "HelveticaBold,17" offset 2,0
set xlabel font "HelveticaBold,17" 

set border 3 lw LINWID

set xtics nomirror
set ytics nomirror

