#
# Style for producing a 1x2 histogram plot.
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

# Include the common style file for 1x2 histogram plots
load "gnuplot-styles/common_style_1X2.gp"

# Initialize the histogram plot
set style data histograms
set style histogram cluster  gap 1

# Set tics
set tics out

# Set the boxwidth (dont change)
set boxwidth .9 relative

# Set the origin of the first plot
P1_OX = 0.0
P1_OY = 0.1

# Set the size of the first plot
P1_SX = 0.5
P1_SY = 0.76

# Set the origin of the second plot
P2_OX = 0.48
P2_OY = 0.1

# Set the size of the second plot
P2_SX = 0.5
P2_SY = 0.76
