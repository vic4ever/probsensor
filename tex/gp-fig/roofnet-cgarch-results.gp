# Turn on macros
set macros

# Load diffrent line chart styles
load "gnuplot-styles/plot_hist_style_1X2.gp"

#
# file name without extension. the final extension is pdf
#
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

## Set font for title
set xrange [-.5:3.5]
set yrange [0:100]

set xtics 1
set ytics 20

set multiplot 

set origin P1_OX, P1_OY
set size P1_SX, P1_SY
set key off
set ylabel "percent captured"
set xlabel "erroneous values"

KEY_TITLE1 = "'{/TimesBold=16 C-GARCH       }'"
KEY_TITLE2 = "'{/TimesBold=16 GARCH}'"

set title '(a)'  
plot 'roofnet-cgarch-results.dat' index 0 u 2 title @KEY_TITLE1 fs solid 0.25 ,\
                               '' index 0 u 3:xticlabels(1) title @KEY_TITLE2 fs pattern 5


set origin P2_OX, P2_OY
set size P2_SX, P2_SY
set key off
set ylabel "average time (sec.)" 
set xlabel "erroneous values"
set yrange [0.0:1.0]
set ytics 0.2
set mytics 


set title '(b)' 
plot 'roofnet-cgarch-results.dat' index 1 u 2 title @KEY_TITLE1 fs solid 0.25 ,\
                               '' index 1 u 3:xticlabels(1) title @KEY_TITLE2 fs pattern 5

#
# Make the outer legend when the key lengths are random and long
#
reset; unset xtics; unset ytics; unset border; unset mxtics; unset mytics
set style data histograms
set style histogram cluster  gap 100000
set boxwidth 0 absolute

KEY_TOPOFFSET = 0.15

KEY_ORIGX1 = 0.1
KEY_ORIGY1 = 0.05

KEY_SIZEX1 = 0.9
KEY_SIZEY1 = (P1_SY + KEY_TOPOFFSET)

set origin KEY_ORIGX1, KEY_ORIGY1 
set size KEY_SIZEX1, KEY_SIZEY1 
set key center top horizontal Left reverse samplen 4

set title ''

plot [][0:0.5] '-' u 1 title @KEY_TITLE1 fs solid 0.25, '-' title @KEY_TITLE2 fs pattern 5 
#plot [][0:0001] 0 title @KEY_TITLE1 w boxes fs solid 0.25 lt 1, \
#             0 title @KEY_TITLE2 w boxes fs empty lt 1
-1000
e
-1000
e
         
unset multiplot
