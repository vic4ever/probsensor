# Load diffrent line chart styles
load "gnuplot-styles/plot_box_style_1X1.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

set key left top horizontal Left reverse samplen 2

set ylabel "elapsed time (sec.)"
set xlabel "window size (samples)"
set xrange[10:200]
set yrange[0:1000]
set xtics 30
set key off

plot 'garch-scale-char.dat' using 1:2 with @BOX_STY2
