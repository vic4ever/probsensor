\appendices \label{sec:appendix}
%% \section{Proofs of Theorems}
%% \label{sec:proofs}
%% \begin{proof}(\textbf{\thmref{thm:error-gar}}) Substituting $\hat{\sigma}_{t'} = d_s \cdot \hat{\sigma}_{t}$ in \eqnref{eqn:hdist} we obtain,
%% \begin{displaymath}
%% (1-\mathcal{H}'^2)\sqrt{1+d_s^2}-\sqrt{2\cdot d_s} = 0.
%% \end{displaymath}
%% Solving for $d_s$ we obtain,
%% \begin{equation}
%% d_s \leq \frac{2+\sqrt{4-4 \left(1-\mathcal{H'}^2\right)^4}}{2 \left(1-\mathcal{H'}^2\right)^2}.
%% \end{equation}
%% Since $d_s$ is monotonically increasing in $\mathcal{H}'$, choosing a value of $d_s$ as given in the above equation guarantees the distance constraint $\mathcal{H}'$.
%% \end{proof}

%% \begin{proof}(\textbf{\thmref{thm:mem-gar}}) From \eqnref{eqn:sigma-step} we obtain,
%% \begin{align}
%% \log_e(max(\hat{\sigma}_t)) & = \mathcal{Q}'\cdot \log_e(d_s) + \log_e(min(\hat{\sigma}_t)) \nonumber \\ 
%% \log_e(d_s) & = \frac{1}{\mathcal{Q}'}\log_e\left( \frac{max(\hat{\sigma}_t)}{min(\hat{\sigma}_t}\right) \nonumber\\
%% d_s & = max(\hat{\sigma}_t)^{\frac{1}{\mathcal{Q}'}} \cdot min(\hat{\sigma}_t)^{-\frac{1}{\mathcal{Q}'}}. \nonumber
%% \end{align}
%% From the above equation we can see that $d_s$ is monotonically decreasing in $\mathcal{Q}'$ and since $\mathcal{D}_s = \frac{max[\hat{\sigma}_t]}{min[\hat{\sigma}_t]}$ we obtain,
%% \begin{equation}
%% d_s \geq \mathcal{D}_s^{\frac{1}{\mathcal{Q}'}}.
%% \end{equation}
%% Choosing a value of $d_s$ as given in the above equation guarantees the memory constraint $\mathcal{Q}'$ because of the monotonicity property of $d_s$.
%% \end{proof}


\section{Additional Experiments}
\label{sec:addtional-exp}
Here we investigate several other aspects of our data. Particularly, we check whether our data exhibits time-varying
volatility. Next, we evaluate the effect of model order on density distance. Lastly, we show that the \garch~model is scalable \wrt~
the window size ($H$).

\stitle{Testing for time-varying volatility}
Before we infer dynamic density using the \armagarch~metric or \kalgarch~metric it is important to verify whether the
underlying time series of raw values follows the characteristics assumed by these metrics. These metrics
model volatility as a function of time, therefore it is important to test whether the underlying time series also exhibits such behavior. 
In time-series literature, a time series in which volatility changes as a function of time is known as \emph{heteroscedastic} \cite{shumway2005}. On the contrary, a time series in which volatility is constant is known as \emph{homoscedastic}. Evaluating whether our datasets show
heteroscedasticity is critical since it justifies our use of volatility estimation models.

For testing heteroscedasticity we use a null hypothesis test proposed in
\cite{shumway2005}. The null hypothesis tests whether the errors  obtained from using a \arma~model ($a^2_i$) are independent and identically distributed (\emph{i.i.d}). The test is
equivalent to testing whether $\xi_j=0 \quad \forall j=1,\ldots, m$ in the linear regression,
\begin{equation}
\label{eqn:nullhyp-reg}
a^2_i = \xi_0 + \xi_1 a^2_{i-1} + \cdots + \xi_m a^2_{i-m} + e_i,
\end{equation}
where $i \in \{m+1,\ldots, K\}$, $e_i$ denotes the error term, $m \geq 1$ and $K$ is the sample size. Thus the null hypothesis becomes $H_0:\xi_1=\ldots=\xi_m=0$.

First, we start by computing the sample mean of $a^2_i$ as $\mu(a^2_i) = \frac{1}{K}\sum_{i=1}^K a^2_i$. Also, let $\gamma_0 = \sum^K_{i=m+1} (a^2_i - \mu(a^2_i))^2$ and $\gamma_1 = \sum_{i=m+1}^K e^2_i$ where $e_i$ are the residuals of the linear regression in \eqnref{eqn:nullhyp-reg}. Then,
\begin{equation}
\Phi(m) = \frac{(\gamma_0-\gamma_1)/m}{\gamma_1 / (K-2m-1)},
\end{equation}
is asymptotically distributed as a chi-square distribution with $m$ degrees of freedom ($\chi^2_m$). Thus
we reject the null hypothesis $H_0$ if $\Phi(m) > \chi^2_{m}(\alpha)$, where $\chi^2_{m}(\alpha)$ is in the upper
$100(1-\alpha)^{th}$ percentile of $\chi^2_m$ or the p-value of $\Phi(m) < \alpha$. In our experiments we choose $\alpha = 0.05$.

\begin{figure}[!h]
    \centering
    \includegraphics[width=1.0\columnwidth]{gp-fig/garch-effect}
    \caption{Testing heteroscedasticity.}
    \label{fig:arch-test}
\end{figure}
%
To show that our datasets exhibit regimes of changing volatility we compute the value of $\Phi(m)$  where $m = \{1,2, \ldots, 10 \}$ on 1800 windows containing 180 samples each (i.e. $H=180$) for \emph{campus-data} and \emph{car-data}. Then we reject the null hypothesis if the average value of $\Phi(m)$ over all windows is greater than $\chi^2_{m}(\alpha)$.

\figref{fig:arch-test} shows the results from this evaluation. Clearly, from \figref{fig:arch-test}($a$) we can reject the null hypothesis for \emph{campus-data} because for all values of $m$ the curve of $\chi^2_{m}(\alpha)$ is much lower than $\Phi(m)$. This means that $a^2_i$ are not \emph{i.i.d} and thus we can find regimes of
changing volatility. But on the other hand, for \emph{car-data} (see \figref{fig:arch-test}($b$)) we can see that
$\chi^2_{m}(\alpha)$ and $\Phi(m)$ are close to each other.  Thus the \emph{car-data} exhibits less heteroscedasticity as compared
to the \emph{campus-data} which implies that \emph{car-data} is much smoother than \emph{campus-data}. Admittedly, we reject the null hypothesis for \emph{car-data} since
$\chi^2_{m}(\alpha)$ is lower than $\Phi(m)$ for all values of $m$. Thus the \emph{car-data} also exhibits regimes of changing
volatility only not as much as the \emph{campus-data}.

The above results establish that our datasets show change of volatility with time, thus justifying the use
of \garch~model. However, the implication of these experiments is not limited to our datasets since it clearly contradicts the assumption
of homoscedasticity (constant volatility) made by previous works \cite{cheng03,Cheng2004}.

\stitle{Effect of model order on density distance}
Here we discuss the effect of model order of an ARMA(p,0) model on density distance. \figref{fig:time-comp}($a$) shows the density
distance obtained by using several metrics when the model order $p$ takes values $\{2,4,6,8\}$. An
important observation from this experiment is that, for the  \armagarch~metric the density distance increases with 
model order. This justifies our choice of a low model order while using the \armagarch~metric.

\stitle{Scaling behavior of GARCH model}
Here we show that the \garch~model exhibits linear scaling \emph{w.r.t.} the window size. To show this we record the time taken by the \garch~model for various window sizes of the \emph{campus-data}. The result from this experiment is shown in \figref{fig:time-comp}($b$). Observe that the time taken for inference using \garch~model increases linearly \wrt~the window size. This behavior distinctly establishes the scalability of \garch~model.
%
\begin{figure}[!h]
\centering
%\subfloat[]{ \includegraphics[width=0.486\columnwidth]{fig/model-order-vs-distance.pdf}}  \hfil % old figures
%\subfloat[]{\includegraphics[width=0.486\columnwidth]{fig/garch-scaling-with-time.pdf}} \hfil % old figures
\subfloat[]{ \includegraphics[width=0.486\columnwidth]{gp-fig/model-order-vs-vol-distance.pdf}}  \hfil
\subfloat[]{ \includegraphics[width=0.486\columnwidth]{gp-fig/garch-scale-char.pdf}}  \hfil
\qquad
\caption{(a) Effect of model order. (b) Scaling behavior of \garch~model.}
\label{fig:time-comp}
\end{figure}
