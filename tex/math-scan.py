#!/usr/bin/python
import sys

all_equations = []

search_for = [ ("\\begin{equation}", "\\end{equation}"),
               ("\\begin{displaymath}", "\\end{displaymath"),
               ("\\begin{eqnarray}", "\\end{eqnarray}"),
               ("\\begin{align}", "\\end{align}")
               ]

for fname in sys.argv[1:]:
    file = open(fname)
    string_file = file.readlines()

    new_file = ''
    for line in string_file:
        try:
            if line.strip()[0] != '%':
                new_file = new_file + line
        except IndexError:
            pass
    all_equations.append("\\newpage \n \\verb|%s| \n" % (fname))
    for sfr in search_for:
        beq = new_file.split(sfr[0])

        for each in beq[1:]:
            eqn = each.split(sfr[1])[0]
            eqn = eqn.strip()
            if eqn != '' and eqn not in all_equations:
                all_equations.append(sfr[0] + "\n" + eqn.strip() + "\n" + sfr[1] + "\n")

    dol = new_file.split("$")
    dolmath = []
    for math in  dol[1:len(dol):2]:
        if math not in dolmath:
            dolmath.append(math)

    for math in dolmath:
        eqn = "\\begin{displaymath}\n" + math + "\n\end{displaymath}\n" 
        all_equations.append(eqn)


header = open("math-scan-header.txt")
print header.read()
print "\\begin{document}\n"
for eqn in all_equations:
    print eqn,

print "\n\\end{document}"

