\section{Dynamic Density Methods}
\label{sec:metrics}

In this section, we first present two relatively simple dynamic density methods that capture evolving probability densities in data streams. We then describe the GARCH method that deals with high time-varying volatility in data streams. Finally, we extend the GARCH model into the so-called C-GARCH method, that can robustly capture evolving probability distributions regardless of the presence of erroneous values in a given data stream.
%that are based on existing work for dealing with uncertain data.


\subsection{Uniform Thresholding Method}
\label{sec:metric-thres}
% \stitle{Uniform Thresholding Metric}
The {\em uniform thresholding method} extends the idea proposed by Cheng~\etal~\cite{cheng2003evaluating,Cheng2004} where the authors proposed a generic evaluation framework for imprecise data. The general idea is that by letting the user define a threshold $u$ which specifies a uniform distribution which we expect to contain an estimated correct output value for the sensor at a timestamp. Based on this method, we need a way to estimate the correct value called \emph{expected true value} from the data. We define the expected true value as follows:

%Cheng~\etal~\cite{cheng2003evaluating,Cheng2004} have proposed a generic evaluation framework over imprecise data.
%The key idea in these studies is to model a raw value as a user-provided uncertainty range in which the corresponding unobservable true value resides. Quality are then evaluated over such uncertainty ranges, instead of the raw values. Our {\em uniform thresholding method} extends this idea for estimating probability distributions by inferring a true value. We define such a true value as:


\begin{definition}[Expected true value]
\label{def:true-value}
Given a probability density function $p_t(R_t)$, the expected true value $\hat{r}_t$ is the expected value of $R_t$, denoted as $\bb{E}(R_t)$.
\end{definition}

For example, in \figref{fig:naive}($a$), at each timestamp, we compute a uniform distribution $[\hat{r}_t-u,\hat{r}_t+u]$ where $u$ is a user-specified value and $\hat{r}_t$ is the expected true value at this timestamp. As illustrated in \figref{fig:naive}($a$), this method assumes that the raw value resides in the bound of the uniform distribution. Another observation is that the uniform thresholding method requires a way to calculate the expected true value from the raw data and the user-defined value $u$.

%Next, the uniform thresholding method takes a user-defined threshold value $u$ to bound uniform distributions, centered on the inferred true value. \figref{fig:naive}($a$) illustrates an example of this process where a user-defined threshold value $u$ is used for specifying the uncertainty ranges. The difference between a true value $\hat{r}_t$ and its corresponding raw value $r_t$ is then assumed to be not greater than $u$.


\begin{figure}[!h]
    \center
    \includegraphics[width=0.6\columnwidth]{fig/naive}
    \caption{Examples of two simple dynamic density methods.}
    \label{fig:naive}
\end{figure}

In order to calculate the expected true value from the data, we employ the the \emph{AutoRegressive Moving Average} (\arma)~model~\cite{shumway2005} which is commonly used for predicting expected values in time series \cite{paq}.
%To infer expected true values, we adopt the \emph{AutoRegressive Moving Average} (\arma)~model~\cite{shumway2005} that is commonly used for predicting expected values in time series \cite{paq}.
More specifically, given a time series $S$ and a sliding window $S^H_{t-1}$,
the \arma~model models  $r_i = \hat{r}_i + a_i$, where $t-H \leq i \leq t-1$ and $a_i$ follows a normal distribution where mean is 0 and  variance is $\sigma^2_a$. From an ARMA(p,q) model, the expected true value $\hat{r}_{t}$ can be calculated as follows:
\begin{equation}
\hat{r}_{t} = \phi_0 + \sum_{j=1}^{p} \phi_j r_{t-j} + \sum_{j=1}^{q} \theta_j a_{t-j},
\end{equation}
%
where $(p,q)$ are non-negative integers denoting the model order, $\phi_1, \ldots, \phi_p$ are autoregressive coefficients, $\theta_1, \ldots, \theta_q$ are moving average coefficients, $\phi_o$ is a constant, and $t > max(p,q)$. 
Interested readers can consider \cite{shumway2005} for more information on how to choose the appropriate model parameters $(p,q)$.
%More details regarding the estimation and choice of the model parameters $(p,q)$ are described in~\cite{shumway2005}.
%
%Here, $\hat{r}_i = \bb{E}(r_i | F_{i-1})$ is the expected true value of $r_i$ given all the information available until time $i-1$ denoted by $F_{i-1}$.

%%$\r_i$ is also known as the conditional mean of the model since it denotes
%% the expected mean value at time $i$ conditioned on the past information $F_{i-1}$.
%% Let $\mu_i = \hat{r}_{i}$ be the expected (true) value at time $i$.
%% We denote an \arma~model with model order $(p,q)$ is as $ARMA(p,q)$.

%More details regarding to the \arma~model estimation are described at Chapter~3 in~\cite{shumway2005}.


\subsection{Variable Thresholding Method}
\label{sec:metric-vary}
% \stitle{Variable Thresholding Metric}

As the name suggests, the {\em variable thresholding method} differs from the uniform thresholding method in that the range of the distribution is not fixed by user but it is calculated from the data. Another difference is that while the uniform thresholding method uses uniform distribution, the variable thresholding method employs normal distribution. The normal distribution inferred by variable thresholding method at time $t$ from a sliding window $S^H_{t-1}$ is as follows: 
\begin{equation}
  p_t(R_t=r_t) = \frac{1}{{\sqrt {2\pi s_t^2}}} e^{-(r_t - \hat{r}_t)^2/2s_t^2},
\end{equation}
where $\hat{r}_t$ is the normal distribution's mean and $s_t$ is its standard deviation. As a result, for the variable thresholding method, we need to compute two parameters: the mean of the distribution $\hat{r}_t$ and its standard deviation $s_t$. The mean is computed using the ARMA model i.e., the mean is the expected true value. The variance is then computed and used to get the standard deviation $s_t$. This variable thresholding method is illustrated in \figref{fig:naive}($b$).

%We propose another dynamic density method, termed {\em variable thresholding method}, that differs in two ways from the uniform thresholding method.
%First, the variable thresholding method works on Gaussian distributions, while the uniform thresholding method is applicable only to uniform distributions.
%Second, unlike the uniform thresholding method, the variable thresholding method does not require the user-defined threshold for specifying uncertainty ranges. 
%Instead, it computes a sample variance $s^2_t$ for a window $S^H_{t-1}$, so that $s^2_t$ is used to model a Gaussian distribution.
%Given $S^H_{t-1}$, the variable thresholding method infers a normal distribution at time $t$ as:
%
%\begin{equation}
%  p_t(R_t=r_t) = \frac{1}{{\sqrt {2\pi s_t^2}}} e^{-(r_t - \hat{r}_t)^2/2s_t^2},
%\end{equation}
%%
%where $\hat{r}_t$ is an expected true value inferred by the \arma~model. \figref{fig:naive}($b$) demonstrates an example of estimating normal distributions based on the variable thresholding method.
%First, the ARMA model infers the expected true values $\hat{r}_t$ that are used as the mean values for the normal distributions.
%It then computes the variances that are used to derive the standard deviations $s_t$.


%\begin{figure}[h!]
%    \center
%    \includegraphics[width=0.7\columnwidth]{fig/metric-vary}
%    \caption{An example of variable thresholding method.}
%    \label{fig:metric-vary}
%\end{figure}


%
%\subsubsection{Particle-Filter Metric}\label{sec:particle}
%\fixme{This subsubsection is incomplete. Please skip while reading}
%Recently, the particle filter has been spotlighted by a rich body of research work, in particular for dealing with uncertain time-varying data~\cite{kanagal08,re08,Tran2009}. This perhaps has two major reasons. First, the particle filter does not make any assumption on posterior probability distribution, but represents arbitrary probability densities with particles, i.e., weighted samples. As a result, it is highly applicable to a variety of applications. Second, it is well-known that the particle filter performs very well in terms of probabilistic inference, once particles are properly placed, weighted and propagated over time.
%
%Inheriting the advantages of the particle filter, we constitute our {\em particle-filter metric} for measuring fidelities.
%The particle filter is a special version of the Bayes filter based on Monte Carlo sampling.
%\fixme{to Saket: a formal description (1 paragraph) here for the particle filter}.
%
%\todo after implementation, technical mistakes found.
%
%\stitle{Inferences for true values and fidelities}
%In contrast to the thresholding metric that uses the ARMA model to infer the most probable true values,
%the particle-filter metric derives the most probable true values and fidelities from particles generated.
%Given a set of particles $C_i=\{c_1,c_2,..c_{|C_i|}\}$ at time $t=i$,
%the most probable true value $r^*_i$ is the value of the particle associated with the highest weight, formally:
%%
%\begin{equation}
%r^*_i = .
%\end{equation}
%%
%Similarly, the fidelity $f(r_i)$ is the weight associated with the particle having the closest value to $r_i$:
%%
%\begin{equation}
%f(r_i) = .
%\label{eqn:particle}
%\end{equation}
%
%\stitle{Storage schema}
%In order to store the necessary information for the particle filter in our framework,
%we employ the approach of model-based views for particle filters, introduced in~\cite{kanagal08}.
%This basically stores a pair of particle and its weight in a relation form, so-called a {\em particle table}, which can be either materialized or non-materialized.
%In our fidelity stream, each raw value is associated with one particle table that includes a set of particles.
%
%\stitle{Example}
%\figref{fig:metric-particle} shows an example of our particle-filter metric and its storage schema. In the fidelity stream,
%each raw value $r_i$ is associated with fidelity elements modeled as a particle table that contains five particles $c_1, c_2, \cdots, c_5$
%(the number of particles is a system parameter).
%Each particle in the particle table is also associated with a weight, reflecting the probability that the value associated with the particle appears.
%Therefore, the particle having the highest weight in a particle table indicates the most probable true value (e.g., $r^*_1$ and $r^*_2$ in the figure).
%Similarly, a fidelity $f(r_i)$ is represented by the weight associated with the particle having the closest value to $r_i$ (e.g., $c_5$ at $t=2$ in \figref{fig:metric-particle}).
%
%\begin{figure}[h!]
%    \center
%    \includegraphics[width=1.0\columnwidth]{fig/metric-particle}
%    \caption{An Example of Particle-Filter Metric}
%    \label{fig:metric-particle}
%\end{figure}
%
%In spite of the various advantages of the particle filter, they have some drawbacks, in terms of using them for computing fidelities.
%To fully harvest the benefits from the particle filter, we need to generate sufficient numbers of particles.
%This requires high storage consumption in each particle table.
%In addition, the number of particle tables also becomes large, if a fidelity stream contains a large number of tuples.
%Therefore, processing queries over a long term (i.e., over a large number of particle tables) causes efficiency problems of the system.
%Furthermore, generating a large number of particles may be problematic for on-line fidelity estimation.
%
%In the following subsection, we will overcome these drawbacks by proposing the GARCH metric.
%
