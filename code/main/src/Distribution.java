public class Distribution {
	private double mean;
	private double sd;

	private int senid;
	private int winid;

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getSd() {
		return sd;
	}

	public void setSd(double sd) {
		this.sd = sd;
	}

	public int getSenid() {
		return senid;
	}

	public void setSenid(int senid) {
		this.senid = senid;
	}

	public int getWinid() {
		return winid;
	}

	public void setWinid(int winid) {
		this.winid = winid;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if (obj instanceof Distribution) {
			Distribution that = (Distribution) obj;
			return this.getMean() == that.getMean()
					&& this.getSd() == that.getSd()
					&& this.getSenid() == that.getSenid()
					&& this.getWinid() == that.getWinid();
		}

		return result;
	}

	@Override
	public String toString() {
		return senid + "," + winid + "," + mean + "," + sd + "\n";
	}
}
