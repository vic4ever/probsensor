import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by Asus on 5/26/14.
 */
public class Main {

    int n = 10; // Number of sensors
    int t = 100; // Number of timestamps
    public Distribution[][] distributions = new Distribution[n][t];
    public double[] betas = new double[n];
    public double[][] alphas = new double[n][t];

    public double[][] oldAlphas = new double[n][t];
    public double[] oldBetas = new double[n];

    double initVal = 0.5;

    public void calculateTrustScores() {
        int q = 1;
        for (int i = 0; i < n; i++) {
            betas[i] = 0.5;
        }
        while (!terminated()) {
            double numerator = 0;
            for (int i = 0; i < n; i++) {
                numerator += betas[i];
            }

//            System.arraycopy( alphas, 0, b, 0, alphas.length );
//            oldAlphas = Arrays.copyOf();

            // Calculate alphas
            for (int l = 0; l < t; l++) {
                // At each time stamp l
                for (int i = 0; i < n; i++) {
                    // At each sensor i
                    Distribution distribution_i = distributions[i][l];

                    double denominator = 0;
                    for (int j = 0; j < n; j++) {
                        // At each sensor j != i
                        if (j != i) {
                            Distribution distribution_j = distributions[j][l];
                            denominator += betas[j] * similarity(distribution_i, distribution_j, l);
                        }
                    }

                    alphas[i][l] = denominator / numerator;
                }
            }

//            oldBetas = betas;
            System.arraycopy(betas, 0, oldBetas, 0, betas.length);

            // Calculate betas
            for (int i = 0; i < n; i++) {
                // At each sensor i
                betas[i] = 0;
                for (int l = 0; l < t; l++) {
                    // At each time stamp l
                    betas[i] += alphas[i][l];
                }
                betas[i] = betas[i] / t;
            }
            q++;
        }

        System.out.print("#Iter: " + q);
    }

    public boolean terminated() {
        double olsSum = 0;
        double newSum = 0;
        for (int i = 0; i < n; i++) {
            olsSum += oldBetas[i];
            newSum += betas[i];
        }
        double avgOld = olsSum / n;
        double avgNew = newSum / n;

        System.out.println(avgNew);

        if (Math.abs(avgNew - avgOld) <= Math.pow(10, -10)) {
            return true;
        } else {
            return false;
        }
    }

    public double distance(Distribution distribution_i, Distribution distribution_j) {
        double u1 = distribution_i.getSd();
        double m1 = distribution_i.getMean();
        double u2 = distribution_j.getSd();
        double m2 = distribution_j.getMean();
        double dist12 = Math.log(u2 / u1) + (Math.pow(u1, 2) + Math.pow(m1 - m2, 2)) / (2 * Math.pow(u2, 2)) - 1 / 2;
        double dist21 = Math.log(u1 / u2) + (Math.pow(u2, 2) + Math.pow(m1 - m2, 2)) / (2 * Math.pow(u1, 2)) - 1 / 2;
        double dist = dist12 + dist21;
        return dist;
    }

    public double diameter(int l) {
        double retval = Double.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            // At each sensor i
            Distribution distribution_i = distributions[i][l];

            for (int j = 0; j < n; j++) {
                // At each sensor j != i
                if (j != i) {
                    Distribution distribution_j = distributions[j][l];

                    double val = distance(distribution_i, distribution_j);
                    ;
                    if (retval < val) {
                        retval = val;
                    }
                }
            }
        }
        return retval;
    }

    public double similarity(Distribution distribution_i, Distribution distribution_j, int l) {
        double diameter = diameter(l);
        double distance = distance(distribution_i, distribution_j);
        return 1 - distance / diameter;
    }

    public Distribution[][] loadDistributions(String meanFile, String sdFile) {
        Distribution[][] distributions = new Distribution[n][t];

        // Load means
        BufferedReader meanBr = null;
        String meanLine = "";
        String cvsSplitBy = ",";
        int senid = -1;

        try {
            meanBr = new BufferedReader(new FileReader(meanFile));
            while ((meanLine = meanBr.readLine()) != null) {
                senid++;
                String[] means = meanLine.split(cvsSplitBy);
                int winid = -1;
                for (String string : means) {
                    winid++;
                    double mean = Double.parseDouble(string);
                    Distribution dist = new Distribution();
                    dist.setMean(mean);
                    dist.setSenid(senid);
                    dist.setWinid(winid);
                    distributions[senid][winid] = dist;
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (meanBr != null) {
                try {
                    meanBr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Load sds
        BufferedReader br = null;
        String line = "";
        senid = -1;
        try {

            br = new BufferedReader(new FileReader(sdFile));
            while ((line = br.readLine()) != null) {
                senid++;
                String[] sds = line.split(cvsSplitBy);
                int winid = -1;
                for (String string : sds) {
                    winid++;
                    double sd = Double.parseDouble(string);
                    Distribution dist = distributions[senid][winid];
                    dist.setSd(sd);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return distributions;
    }

    public static void main(String[] args) {
        Main prob = new Main();
        prob.distributions = prob.loadDistributions("C:/Users/Asus/workspace/ProbSensor/data/means_0.csv",
                "C:/Users/Asus/workspace/ProbSensor/data/sds_0.csv");
        prob.calculateTrustScores();
    }
}
