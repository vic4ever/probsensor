function dist = KL( m1,u1,m2,u2 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    dist12 = log(u2/u1) + (u1^2+(m1-m2)^2)/(2*u2^2) - 1/2;
    dist21 = log(u1/u2) + (u2^2+(m1-m2)^2)/(2*u1^2) - 1/2;
    dist = dist12 + dist21;
end

