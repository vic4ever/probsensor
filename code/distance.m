function dist = distance( m1,u1,m2,u2,fun)
    if fun == 1
        dist = KL(m1,u1,m2,u2);
    end
    if fun == 2
        dist = HL(m1,u1,m2,u2);
    end
    if fun == 3
        dist = bha(m1,u1,m2,u2);
    end
end

