numspammer = 0;
numsensor = 10 - numspammer;
winsize = 100;
%x = [0, 5, 10, 15, 20, 25];
%y = [0, 5, 10, 15, 20, 25];
x = [0];
y = [0];

path = 'C:\Users\Asus\workspace\ProbSensor\data\';
%path = strcat(path,num2str(numspammer),'\');

% numsensor * winsize * (mean, sd)
distributions = zeros(numsensor,winsize,2);

true_mean = random('Normal',0.5,0.2,1,winsize);
true_sd = random('Normal',0.15,0.05,1,winsize);

% Only contain distribution at each timestamp (no sensor here)
csvwrite(strcat(path,'true_means.csv'),true_mean);
csvwrite(strcat(path,'true_stds.csv'),true_sd);

%%% Add noise to each mean/sd to get mean and sd for each sensors
%%% Generate real sensors
mean_noises = random('Normal',0.1,0.005,numsensor,winsize);
sd_noises = random('Normal',0.05,0.005,numsensor,winsize);

for winid = 1:winsize
    means = true_mean(winid) + mean_noises(:,winid);
    sds = true_sd(winid) +  sd_noises(:,winid);
    distributions(:,winid,1) = means;
    distributions(:,winid,2) = sds;
end

distributions_out = zeros(numsensor,winsize,2);
%%% Store real sensors distributions 
for x_id = 1:size(x,2)
    %%% Add % to mean
    xp = repmat((100+x(x_id))/100,numsensor,winsize);
    distributions_out(:,:,1) = xp .* distributions(:,:,1);
    
    csvwrite(strcat(path,'means_', num2str(x(x_id)), '.csv'),distributions_out(:,:,1));
end

for y_id = 1:size(y,2)
    %%% Add % to std
    yp = repmat((100+y(y_id))/100,numsensor,winsize);
    distributions_out(:,:,2) = yp .* distributions(:,:,2);
    
    csvwrite(strcat(path,'sds_' , num2str(y(y_id)), '.csv'),distributions_out(:,:,2));
end

%%% Generate spammer sensors
%spam_means = rand(numspammer,winsize);
%b = 0.2;
%a = 0.1;
%spam_stds = a + (b-a).*rand(numspammer,winsize);


% Store spammer means and stds
%csvwrite(strcat(path,'spam_means.csv'),spam_means);
%csvwrite(strcat(path,'spam_stds.csv'),spam_stds);

%%% Calculate distance between distributions
% KLdistances = zeros(numsensor,numsensor,winsize);
% skl = cputime;
% for winid = 1:winsize
%     for senid1 = 1:numsensor
%         m1 = distributions(senid1,winid,1);
%         u1 = distributions(senid1,winid,2);
%         for senid2 = senid1:numsensor
%             m2 = distributions(senid2,winid,1);
%             u2 = distributions(senid2,winid,2);
%             KLdistances(senid1,senid2,winid) = KL(m1,u1,m2,u2);
%         end
%     end
% end
% ekl = cputime - skl;
% 
% HLdistances = zeros(numsensor,numsensor,winsize);
% shl = cputime;
% for winid = 1:winsize
%     for senid1 = 1:numsensor
%         m1 = distributions(senid1,winid,1);
%         u1 = distributions(senid1,winid,2);
%         for senid2 = senid1:numsensor
%             m2 = distributions(senid2,winid,1);
%             u2 = distributions(senid2,winid,2);
%             HLdistances(senid1,senid2,winid) = HL(m1,u1,m2,u2);
%         end
%     end
% end
% ehl = cputime - shl;
% 
% bhadistances = zeros(numsensor,numsensor,winsize);
% sbh = cputime;
% for winid = 1:winsize
%     for senid1 = 1:numsensor
%         m1 = distributions(senid1,winid,1);
%         u1 = distributions(senid1,winid,2);
%         for senid2 = senid1:numsensor
%             m2 = distributions(senid2,winid,1);
%             u2 = distributions(senid2,winid,2);
%             bhadistances(senid1,senid2,winid) = bha(m1,u1,m2,u2);
%         end
%     end
% end
% ebh = cputime - sbh;