path_original = 'C:\Users\Asus\workspace\ProbSensor\data\data\';

for i=1:100
    numspammer = 0;
    numsensor = 10 - numspammer;
    winsize = 100;
    %x = [0, 5, 10, 15, 20, 25];
    %y = [0, 5, 10, 15, 20, 25];
    x = [10, 20, 30, 40 , 50 ,60 ,70, 80, 90, 100];
    y = [10, 20, 30, 40 , 50 ,60 ,70, 80, 90, 100];
    %x = [0];
    %y = [0];
    path = strcat(path_original,num2str(i),'\');
    
    %path = strcat(path,num2str(numspammer),'\');
    
    % numsensor * winsize * (mean, sd)
    distributions = zeros(numsensor,winsize,2);
    
    true_mean = random('Normal',0.5,0.2,1,winsize);
    true_sd = random('Normal',0.15,0.05,1,winsize);
    
    %true_mean = awgn(true_mean,5);
    %true_sd = awgn(true_sd,5);
    
    % Only contain distribution at each timestamp (no sensor here)
    csvwrite(strcat(path,'means\','true_means.csv'),true_mean);
    csvwrite(strcat(path,'means\','true_stds.csv'),true_sd);
    
    csvwrite(strcat(path,'variance\','true_means.csv'),true_mean);
    csvwrite(strcat(path,'variance\','true_stds.csv'),true_sd);
    
    %spam_stds = a + (b-a).*rand(numspammer,winsize);
    
    for x_id = 1:size(x,2)
        for winid = 1:winsize
            a = (1-x(x_id)/100).*true_mean(winid);
            b = (1+x(x_id)/100).*true_mean(winid);
            means = a + (b-a).*rand(numsensor,1);
            %means = true_mean(winid);
            size(means)
            sds = true_sd(winid);
            distributions(:,winid,1) = means;
            distributions(:,winid,2) = sds;
        end
        csvwrite(strcat(path,'means\','means_', num2str(x(x_id)), '.csv'),distributions(:,:,1));
        csvwrite(strcat(path,'means\','stds_' , num2str(x(x_id)), '.csv'),distributions(:,:,2));
    end
    
    
    for x_id = 1:size(x,2)
        for winid = 1:winsize
            means = true_mean(winid);
            a = (1-x(x_id)/100).*true_sd(winid);
            b = (1+x(x_id)/100).*true_sd(winid);
            sds = a + (b-a).*rand(numsensor,1);
            distributions(:,winid,1) = means;
            distributions(:,winid,2) = sds;
        end
        csvwrite(strcat(path,'variance\','means_', num2str(y(x_id)), '.csv'),distributions(:,:,1));
        csvwrite(strcat(path,'variance\','stds_' , num2str(y(x_id)), '.csv'),distributions(:,:,2));
    end
end

% distributions_out = zeros(numsensor,winsize,2);
% %%% Store real sensors distributions
% for x_id = 1:size(x,2)
%     %%% Add % to mean
%     xp = repmat((100+x(x_id))/100,numsensor,winsize);
%     distributions_out(:,:,1) = xp .* distributions(:,:,1);
%
%     csvwrite(strcat(path,'means_', num2str(x(x_id)), '.csv'),distributions_out(:,:,1));
% end
%
% for y_id = 1:size(y,2)
%     %%% Add % to std
%     yp = repmat((100+y(y_id))/100,numsensor,winsize);
%     distributions_out(:,:,2) = yp .* distributions(:,:,2);
%
%     csvwrite(strcat(path,'sds_' , num2str(y(y_id)), '.csv'),distributions_out(:,:,2));
% end
