\subsection{Creating Probabilistic Databases}\label{sec:query}
\fixme{add little intro. say that we make BID databases}

In \secref{sec:prob} we defined the probability estimation query for creating probabilistic databases (see
\defref{def:prob-esti-query}). This query considers only a fixed set of ranges given by $C$. But, the distributions obtained from
\garch~model changes \wrt~time. For this reason we have to modify the query such that the ranges are relatively fixed to each other but
change in absolute value over time. 

Let $S = \langle r_1,r_2,\ldots,r_{t_m}\rangle$ be a time series. As usual, first we start by executing the
\armagarch~algorithm for all sliding windows $S^H_{t-1}$ where $H \leq t \leq t_m$. This gives us $p_t(R_t)$ where $H \leq t \leq
t_m$. Then, we define the ranges relative to mean ($\hat{\mu}_t$) of the time-varying probability distribution. Particularly, for
$p_t(R_t) \sim \mathcal{N}(\hat{\mu}_t,\hat{\sigma}^2_t)$ we define $C = \{\hat{\mu}_t + \Delta\lambda | \lambda = -\frac{n}{2},\ldots,
\frac{n}{2}\}$ where $\Delta \in \mathbb{R}_+$, $n \geq 0$ and even. Simply said, we define a set of intervals around
$\hat{\mu}_t$ such that there are $n$ such intervals each having length $\Delta$. The parameters $n$ and $\Delta$ should be
provided by the user at query time since they specify the granularity of the required database.

\begin{definition}\textbf{probabilistic database creation query.} \\
Given a time interval $t_1 \leq t \leq t_2$, number of intervals $n$, and granularity $\Delta$. A probabilistic database
creation query returns for each $t$ between $t_1$ and $t_2$, a set of probabilities $\Lambda_t = \{\rho_\lambda| \lambda = -\frac{n}{2}, \ldots,
(\frac{n}{2}-1)\}$ where $\rho_\lambda = \int_{\hat{\mu}_t + \lambda\Delta}^{\hat{\mu}_t + (\lambda+1)\Delta} p_t(R_t)$.
\end{definition}
Moreover, this query could be thought of as creating a materialized view over the table of raw values. \figref{fig:create-pdb}
shows an example of creating a probabilistic view\footnote{We use \emph{probabilistic view} and \emph{probabilistic database}
  interchangeably.} using a table of raw values. A probabilistic view is created on the table \texttt{raw\_values} which stores the
output obtained from the \garch~model for sensor \texttt{sid}. The query is given as follows:
\begin{verbatim}
  CREATE VIEW probdb USING INTERVALS 
  delta=2, n=2 FROM raw_values 
  WHERE t >= 1 AND t <= 4 GROUP BY t
\end{verbatim}

This query creates a view \texttt{probdb} with $\Delta=2$, $n=2$, and $1 \leq t \leq 4$. Notice that this query could be easily
extended to a table storing values for more than one sensor by adding one (or more) predicates to the \texttt{WHERE} and
\texttt{GROUP BY} clauses. 

Recall, for answering the above query we need to compute for each $t$ satisfying the \texttt{WHERE} clause, 
\begin{align}
\rho_\lambda &= \int_{\hat{\mu}_t + \lambda\Delta}^{\hat{\mu}_t + (\lambda+1)\Delta} p_t(R_t)\nonumber \\
         &= P_t(R_t = \hat{\mu}_t + \Delta(\lambda+1)) - P_t(R_t = \hat{\mu}_t + \Delta\lambda),
\end{align}
where $ \lambda = \{-\frac{n}{2}, \ldots, (\frac{n}{2}-1)\}$ and $P_t(R_t)$ is a cumulative distribution function of $r_t$. 
Moreover, given values of $\Delta$ and $n$ we only need to compute the values of $P_t(R_t = \hat{\mu}_t + \Delta(\lambda+1))$ and $P_t(R_t = \hat{\mu}_t + \Delta\lambda)$. 

The next challenge is to obtain the query response as efficiently as possible. This is an important aspect of query processing 
since we could easily expect queries where $t$ could span over many days comprising of millions of raw values. Moreover, this task becomes
further challenging when the query requests for a view with higher granularity. We expect this to happen since many applications 
would like to reason about smaller units of data. And, in some cases a probabilistic view of lower granularity could be created
using the one of higher granularity. Thus, having a probabilistic view of higher granularity is advantageous. To the contrary,
high values of $t$, $\Delta$, and $\lambda$ significantly increase the computations performed.

Clearly, it would be advantageous if we could cache and reuse some of the computation for $\Lambda_t$ we performed for earlier times $t$
to compute the same for later times. For doing this we observe that although $p_t(R_t)$ exhibits temporal changes, in many cases it does not
change very rapidly or it exhibits periodic behavior. For example, a temperature sensor follows roughly the same distribution during
early morning hours every day. Thus, a probabilistic view query over several days of data could take advantage of this periodic
behavior to reduce computation. With this intuition, we propose an caching technique which we call $\sigma$--caching. It improves
the performance of query evaluation by efficiently caching the values of $\Lambda_t$.

\begin{figure}[h!]
    \center
    \includegraphics[width=1.0\columnwidth]{gp-fig/create-pdb-query-ex}
    \caption{Example of creating a probabilistic view using $\sigma$--cache.}
    \label{fig:create-pdb}
\end{figure}
\stitle{Caching Gaussian distributions using $\sigma$--cache}
We know that $P_t(R_t)$ is a Gaussian cumulative distribution function of $r_t$ at time $t$. Let us denote this function as $F_G(r_t;\hat{\mu}_t,\hat{\sigma}^2_t)$. 
Moreover, the shape of the function $F_G(r_t;\hat{\mu}_t,\hat{\sigma}^2_t)$ only depends on $\hat{\sigma}^2_t$, since $\hat{\mu}_t$ is a location
parameter. For example, suppose we advance and move to time $t'$ where $t' > t$ then $P_{t'}(R_t) =
F_G(r_{t'};\hat{\mu}_{t'},\hat{\sigma}^2_{t'})$. Observe that, for any given values of $\Delta$ and $\lambda = \{-\frac{n}{2}, \ldots,
(\frac{n}{2}-1)\}$, the values of $F_G(\hat{\mu}_t +\Delta\lambda;\hat{\mu}_t,\hat{\sigma}^2_t)$ and $F_G(\hat{\mu}_{t'}+\Delta\lambda;\hat{\mu}_{t'},\hat{\sigma}^2_{t'})$ are the same if $\hat{\sigma}_t$ is equal to
$\hat{\sigma}_{t'}$. Moreover, since in our case the values of $\Delta$ and $\lambda$ are fixed while evaluating a query, the values $F_G(\hat{\mu}_t+\Delta\lambda;\hat{\mu}_t, \hat{\sigma}^2_t)$ and
$F_G(\hat{\mu}_{t'}+\Delta\lambda;\hat{\mu}_{t'}, \hat{\sigma}^2_{t'})$ solely depend on $\hat{\sigma}_t$ and $\hat{\sigma}_{t'}$ respectively.
%% Now, if $\hat{\sigma}_1$ is equal to $\hat{\sigma}_2$ then the shape of the distrbution is preserved under mean shifts. Thus, $F_G(\hat{\mu}_1+\Delta\lambda;\hat{\mu}_1,
%% \hat{\sigma}^2_1)$ and $F_G(\hat{\mu}_2+\Delta\lambda;\hat{\mu}_2, \hat{\sigma}^2_2)$ only depend on $\Delta$, $\lambda$, $\hat{\sigma}^2_1$, $\hat{\sigma}^2_2$. And, since the values of
%% $\Delta$ and $\lambda$ are constant while evaluating a query, the values of $F_G(\hat{\mu}_1+\Delta\lambda;\hat{\mu}_1, \hat{\sigma}^2_1)$ and
%% $F_G(\hat{\mu}_2+\Delta\lambda;\hat{\mu}_2, \hat{\sigma}^2_2)$ soley depend on $\hat{\sigma}^2_1$ and $\hat{\sigma}^2_2$. 

In general, given two cumulative distribution functions (\emph{cdf}s) $F_G(r_t;\hat{\mu}_t,\hat{\sigma}_t^2)$ and $F_G(r_{t'};\hat{\mu}_{t'},\hat{\sigma}_{t'}^2)$ we are interested in knowing how the
Euclidean distance between them behaves as a function of $\hat{\sigma}_t$ and $\hat{\sigma}_{t'}$. If we know this relationship, we can use it
to find out whether the values of $F_G(\hat{\mu}_t +\Delta\lambda;\hat{\mu}_t,\hat{\sigma}^2_t)$ and
$F_G(\hat{\mu}_{t'}+\Delta\lambda;\hat{\mu}_{t'},\hat{\sigma}^2_{t'})$, and subsequently $\Lambda_t$ and $\Lambda_{t'}$, are close to each other. In this respect we prove the following lemma in the Appendix:
\begin{lemma}
\label{lem:prob-dist}
Given two Gaussian cdfs $F_G(x_1;\mu_1,\sigma_1^2)$ and \linebreak $F_G(x_2;\mu_2,\sigma_2^2)$ the Euclidean
distance between them, \linebreak $\sqrt{(F_G(x_1;\mu_1,\sigma_1^2) - F_G(x_2;\mu_2,\sigma_2^2))^2}$, is directly proportional to $\Bigl\lvert\frac{\sigma_2 - \sigma_1}{\sigma_1\sigma_2}\Bigl\lvert$ for given values of $\Delta$ and $\lambda = \{-\frac{n}{2}, \ldots,(\frac{n}{2}-1)\}$.
\end{lemma}
We use the above lemma to efficiently store and reuse values of $P_t(R_t)$ while query processing. More specifically, we store the
values of $\Lambda_t$ obtained for a particular $P_t(R_t)$ in a B-tree along with key $\hat{\sigma}_t$. Then when we want to compute
$P_{t'}(R_{t'})$ we first look up the B-tree to find $\hat{\sigma}_t^*$ such that $\Bigl\lvert\frac{\hat{\sigma}_{t'} -
  \hat{\sigma}^*_{t}}{\hat{\sigma}_{t'}\hat{\sigma}^*_t}\Bigl\lvert \leq d_\sigma$. If we successfully find such a
$\hat{\sigma}_t^*$ then the corresponding values $\Lambda_t^*$ are used for $P_{t'}(R_{t'})$. Otherwise, the values are newly
computed and stored in the B-tree with key $\hat{\sigma}_{t'}$. Here, $d_\sigma \in \mathbb{R}_+$ is a distance threshold
parameter which generally is set to very small values. Thus, with this approach we achieve significant reuse of computation while
answering probabilistic view queries. Moreover, In \secref{sec:exp} we will show that this simple approach shows manyfold increase
in the performance of query processing.

\begin{figure}[h!]
    \center
    \includegraphics[width=1.0\columnwidth]{gp-fig/sigma-index-const}
    \caption{Example illustrating that area under the curve of a Gaussian cumulative probability distribution is conserved under
      mean shifts. \fixme{reduce size}}
    \label{fig:sigma-ind}
\end{figure}

\fixme{explain how can other queries with other delta and be answered}
