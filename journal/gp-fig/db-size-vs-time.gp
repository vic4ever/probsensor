# Load diffrent line chart styles
load "gnuplot-styles/plot_hist_style_1X1.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

set key left top horizontal Left reverse samplen 2

set ylabel "time (milliseconds)"
set xlabel "database size (tuples)"

set xrange [0.5:4.5]
set ytics 400

plot 'db-size-vs-time-and-error.dat' index 0 using 4 title '{/TimesBold=21 naive}' fs solid 0.5, \
                                 '' index 0 using 3:xticlabels(1) title '{/TimesBold=21 {/Symbol s}-cache}' fs pattern 5

