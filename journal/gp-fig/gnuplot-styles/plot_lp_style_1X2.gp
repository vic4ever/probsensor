#
# Style for producing a 1x2 line points plot
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

# Include the common style file for 1x2 plots
load "gnuplot-styles/common_style_1X2.gp"

# Initialize common styles for line points
LP_STY1 = "lp lt 1 lw LINWID ps PNTSIZ pt 10"
LP_STY2 = "lp lt 1 lw LINWID ps PNTSIZ pt 2"
LP_STY3 = "lp lt 1 lw LINWID ps PNTSIZ pt 3"
LP_STY4 = "lp lt 1 lw LINWID ps PNTSIZ pt 4"

# Set the origin of the first plot
P1_OX = 0.0
P1_OY = 0.1

# Set the size of the first plot
P1_SX = 0.5
P1_SY = 0.76

# Set the origin of the second plot
P2_OX = 0.48
P2_OY = 0.1

# Set the size of the second plot
P2_SX = 0.5
P2_SY = 0.76



