#
# Style for producing a 1x1 histogram plot
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

# Include the common style file for 1x1 plots
load "gnuplot-styles/common_style_1X1.gp"

# Initialize histogram parameters
set style data histograms
set style histogram cluster  gap 1

# Set tics
set tics out 

# Set width of boxes used
set boxwidth .9 relative



