#
# This is a common style file for producing a 1X1 (single) plot
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

# Line width and point size parameters
LINWID = 1.5
PNTSIZ = 1.45

# Turn on macros
set macros

# Set a postscript terminal
set term postscript eps size 3in,2.5in fontfile "/usr/share/texmf/fonts/type1/bluesky/cm/cmsy10.pfb"
set termoption enhanced

# Turn off top and right borders
set border 3 lw LINWID

# Set tics font
set xtics nomirror font "Helvetica,20"
set ytics nomirror font "Helvetica,20"

## Set font size and offset for labels
set ylabel font "HelveticaBold,25" offset -0.8,0
set xlabel font "HelveticaBold,25" offset 0,-1.2