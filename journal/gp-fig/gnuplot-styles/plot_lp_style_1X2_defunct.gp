# Turn on macros
set macros

# Load diffrent line chart styles
load "chart_style.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set term postscript eps

set termoption enhanced

set output OP_FILE

## Set font for title
set xrange [30:180]
set yrange [0.0:3.0]

set xtics 30

set multiplot 

set origin 0.0, 0.05
set size 0.47, 0.55
set key off
set ylabel "density distance"
set xlabel "window size (samples)"

set title '(a) campus-data'  offset 0,-21 font "TimesItalic,25"
plot "train-size-vs-vol-distance" index 0 u 1:2 title "UT" w @STY1 ,\
     "train-size-vs-vol-distance" index 0 u 1:3 title "VT" w @STY2 , \
     "train-size-vs-vol-distance" index 0 u 1:4 title "ARMA-GARCH" w @STY3, \
     "train-size-vs-vol-distance" index 0 u 1:5 title "Kalman-GARCH" w @STY4

set origin 0.48, 0.05
set size 0.47, 0.55
set key off
set ylabel "density distance" 
set xlabel "window size (samples)"

set title '(b) car-data' offset 0,-21 font "TimesItalic,25"
plot "train-size-vs-vol-distance" index 2 u 1:2 title "UT" w @STY1 , \
     "train-size-vs-vol-distance" index 2 u 1:3 title "VT" w @STY2 , \
     "train-size-vs-vol-distance" index 2 u 1:4 title "ARMA-GARCH" w @STY3 , \
     "train-size-vs-vol-distance" index 2 u 1:5 title "Kalman-GARCH" w @STY4

#
# Make the outer legend when the key lengths are roughly the same and small
#
# reset; unset xtics; unset ytics; unset border;
# set size 1,1
# set origin 0,0
# set key center top horizontal Left reverse samplen 2
# set tmargin 13
# set lmargin 1
# set rmargin 5
# set title ''

# plot [][0:1] 2 title '{/TimesBold=16 UT   }' w lp lt LINSTY lw LINWID ps PNTSIZ pt 10,\
#      2 title '{/TimesBold=16 VT   }' w lp lt LINSTY lw LINWID ps PNTSIZ, \
#      2 title '{/TimesBold=16 AR   }' w lp lt LINSTY lw LINWID ps PNTSIZ, \
#      2 title '{/TimesBold=16 Ka   }' w lp lt LINSTY lw LINWID ps PNTSIZ


#
# Make the outer legend when the key lengths are random and long
#
reset; unset xtics; unset ytics; unset border;
set origin 0.0, 0.05
set size 0.47, 0.62
set key center top horizontal Left reverse samplen 2

set title ''


plot [][0:1] 2 title '{/TimesBold=16 UT       }' w @STY1 ,\
             2 title '{/TimesBold=16 VT }' w @STY2

reset; unset xtics; unset ytics; unset border;
set origin 0.35, 0.05
set size 0.6, 0.62
set key center top horizontal Left reverse samplen 2

set title ''


plot [][0:1] 2 title '{/TimesBold=16 ARMA-GARCH     }' w @STY3, \
             2 title '{/TimesBold=16 Kalman-GARCH}' w @STY4


unset multiplot

# F1 = OP_FILE.'.eps'
# F2 = OP_FILE.'1.pdf'
# F3 = OP_FILE.'.pdf'
# !\rm @F2 @F3
# !ps2pdf @F1 @F2  
# !pdfcrop --margins 0 @F2 @F3
# !\rm @F2