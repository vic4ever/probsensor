# Load diffrent line chart styles
load "gnuplot-styles/plot_box_style_1X1.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

set key left top horizontal Left reverse samplen 2
set logscale y
set ylabel "cache size (kilobytes)"
set xlabel "Hellinger distance"
set xrange[-0.04:0.55]
set yrange[10:2000]
#set ytics 3000
set xtics (0.01,0.1,0.2,0.3,0.4,0.5)
set key off

plot 'db-size-vs-time-and-error.dat' index 2 using 1:4 w @BOX_STY2
