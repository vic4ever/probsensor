# Load diffrent line chart styles
load "gnuplot-styles/plot_box_style_1X1.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

set key left top horizontal Left reverse samplen 2

set ylabel "average distance"
set xlabel "database size (tuples)"
set xrange[2000:22000]
#set yrange[0:0.007]
set yrange[0:0.04]
set ytics 0.01
set xtics 4000
set key off

plot 'db-size-vs-time-and-error-auto-old.dat' index 1 using 1:2 with @BOX_STY2
