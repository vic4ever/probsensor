\documentclass[11pt,twocolumn]{article}
\usepackage{amsmath,epsfig}
%\usepackage{times}
\usepackage{marvosym}
\usepackage{url}
\usepackage{comment}
\usepackage{algorithmicx}
\usepackage[noend]{algpseudocode}
\usepackage{algorithm}
\usepackage{multirow}
\usepackage{mathrsfs}
\usepackage{subfig}
\usepackage{amsfonts}
% TODO in the future: particle filter, theoretical analysis for fidelity metrics, query processing

\newcommand{\stitle}[1]{\vspace*{0.5em}\noindent{\bf #1.\/}$\\$ }
\newcommand{\sstitle}[1]{\vspace*{0.4em}\noindent{\bf #1:\/}}
\newcommand{\todo}{\vspace*{0.4em}\noindent{\bf [To be rewritten] }}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\newcommand{\algorithmicinput}{\textbf{Input:}}
\newcommand{\algorithmicoutput}{\textbf{Output:}}
\newcommand{\algorithmicdescription}{\textbf{Description:}}
\newcommand{\algorithmicbreak}{\textbf{break}}
\newcommand{\algorithmiccontinue}{\textbf{continue}}
%\newcommand{\algorithmicreturn}{\textbf{return~}}
\newcommand{\INPUT}{\item[{\algorithmicinput}]$\phantom{1}$}
\newcommand{\OUTPUT}{\item[\algorithmicoutput]$\phantom{1}$}
\newcommand{\DESCRIPTION}{\item[\algorithmicdescription]$\phantom{1}$\\}
\newcommand{\BREAK}{\State{\algorithmicbreak}}
\newcommand{\CONTINUE}{\State{\algorithmiccontinue}}
\newcommand{\RETURN}{\State{\algorithmicreturn}~}

\newcommand{\etal}{\emph{et al.}}
\newcommand{\secref}[1]{Section~(\ref{#1})}
\newcommand{\figref}[1]{Figure~\ref{#1}}
%\newcommand{\figref}[1]{Fig.~(\ref{#1})}

\newcommand{\tabref}[1]{Table~(\ref{#1})}
\newcommand{\eqnref}[1]{(\ref{#1})}
\newcommand{\algoref}[1]{Algorithm~\ref{#1}}
\newcommand{\fixme}[1]{(\textbf{\IroningII~\emph{#1}})}
\newcommand{\bldm}[1]{\mbox{\boldmath$#1$}}
\newcommand{\scr}[1]{\mathscr{#1}}
%\newcommand{\ourgarch}{\emph{C-GARCH}}
%\newcommand{\garch}{\emph{GARCH}}
%\newcommand{\arma}{\emph{ARMA}}
\newcommand{\ourgarch}{C-GARCH}
\newcommand{\garch}{GARCH}
\newcommand{\arma}{ARMA}

\newcommand{\armagarch}{ARMA-GARCH}
\newcommand{\kalgarch}{Kalman-GARCH}
\newcommand{\bb}[1]{\mathbb{#1}}
%\setlength{\textfloatsep}{2ex} \setlength{\intextsep}{1.2ex}
%\setlength{\dbltextfloatsep}{2ex} \addtolength{\topskip}{-3.0mm}
%\setlength{\textheight}{9.30in}

%\def\sharedaffiliation{%
%\end{tabular}
%\begin{tabular}{c}}


