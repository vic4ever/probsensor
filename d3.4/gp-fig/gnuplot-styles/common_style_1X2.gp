#
# This is a common style file for producing a 1X2 (single) plot
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

LINWID = 1.5
PNTSIZ = 1.45

# Turn on macros
set macros

# Set a postscript terminal
set term postscript eps size 5in,2.7in
set termoption enhanced

# Turn off top and right borders
set border 3 lw LINWID

# set tics font
set xtics nomirror font "Helvetica,15"
set ytics nomirror font "Helvetica,15"

## Set font size and offset for labels
set ylabel font "HelveticaBold,20" offset 0.45,0
set xlabel font "HelveticaBold,20" offset 0,-0.45

## set title
set title offset 0,-23 font "TimesItalic,25"
