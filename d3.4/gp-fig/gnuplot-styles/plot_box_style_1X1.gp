#
# Style for producing a 1x1 bar chart
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

# Include the common style file for 1x1 plots
load "gnuplot-styles/common_style_1X1.gp"

# Set tics
set tics out 

# Set the box width (dont change)
set boxwidth .5 relative

# Different box plot styles 
BOX_STY1 = "boxes fs solid"
BOX_STY2 = "boxes fs solid 0.25"


