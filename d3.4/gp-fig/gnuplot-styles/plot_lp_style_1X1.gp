#
# Style for producing a 1x1 line points plot
# Avoid changing settings here, if required, you can overide them in you gnuplot script
#

# Include the common style file for 1x1 plots
load "gnuplot-styles/common_style_1X1.gp"

# Initialize common styles for line points
LP_STY1 = "lp lt 1 lw LINWID ps PNTSIZ pt 10"
LP_STY2 = "lp lt 1 lw LINWID ps PNTSIZ pt 2"
LP_STY3 = "lp lt 1 lw LINWID ps PNTSIZ pt 3"
LP_STY4 = "lp lt 1 lw LINWID ps PNTSIZ pt 4"


