# Load diffrent line chart styles
load "gnuplot-styles/plot_box_style_1X1.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'
set output OP_FILE

set key left top horizontal Left reverse samplen 2
set ylabel "cache size (kilobytes)"
set xlabel "max. ratio threshold {/TimesItalicBold ({/CMSY10 D}_s)}"
set xrange[1500:21000]
set logscale x
set yrange[850:1150]
set ytics 50
set xtics (2000,4000,8000,16000)
set key off

plot 'db-size-vs-time-and-error.dat' index 3 using 3:6 w @BOX_STY2
