#!/usr/bin/python


import getopt, sys, commands, os

PDFCROP = '/usr/bin/pdfcrop'
PS2PDF = '/usr/bin/ps2pdf'

def main():
    paramDefaults = { 'input' : '/tmp/plot.eps' , 
                      'output' : '/tmp/plot-final.pdf'
                      }

    options = { 'input': ("-i", "--input"),
                'output': ("-o", "--output"),
                'help': ("-h", "--help")
                }

    config = {}

    for param in paramDefaults:
        config[param] = paramDefaults[param]

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:i:v", ["help", "output="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    output = None
    verbose = False

    for o, a in opts:
        if o == "-v":
            verbose = True
        
        elif o in options['help']:
            usage()
            sys.exit()

        elif o in options['input']:
            config['input'] = a

        elif o in options['output']:
            config['output'] = a

        else:
            assert False, "unhandled option"

    op_dir = "/".join(x for x in config['output'].split("/")[:-1])
    op_file = config['output'].split("/")[-1]

    if op_dir == '.':
        op_dir = os.getcwd() 

    cmd = '%s %s /tmp/tmp_%s' % (PS2PDF, config['input'], op_file)
    print 'Executing: %s' % (cmd)
    print commands.getoutput(cmd)
    
    cmd = 'cd /tmp; %s -m 0 tmp_%s; cd -' % (PDFCROP, op_file)
    print 'Executing: %s' % (cmd)
    print commands.getoutput(cmd)
    
    cmd = 'mv /tmp/tmp_%s-crop%s %s/%s' % (op_file[:-4], op_file[-4:], op_dir, op_file)
    os.system('echo -e "\\e[31;1mExecuting: %s \\e[0m"' % (cmd))
    print commands.getoutput(cmd)

if __name__ == "__main__":
    main()
