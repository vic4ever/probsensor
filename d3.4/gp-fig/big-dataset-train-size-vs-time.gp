# Turn on macros
set macros

# Load diffrent line chart styles
load "gnuplot-styles/plot_lp_style_1X2.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

## Set font for title
set xrange [30:180]
set yrange [-1:5.0]

set xtics 30
set ytics 2
set mytics 

set multiplot 

set origin P1_OX, P1_OY
set size P1_SX, P1_SY
set key off
set ylabel "average time (sec.)"
set xlabel "window size (samples)"

KEY_TITLE1 = "'{/TimesBold=16 UT     }'"
KEY_TITLE2 = "'{/TimesBold=16 VT}'"
KEY_TITLE3 = "'{/TimesBold=16 ARMA-GARCH     }'"
KEY_TITLE4 = "'{/TimesBold=16 Kalman-GARCH}'"

set title '(a) campus-data'  
plot "big-dataset-train-size-vs-time.dat" index 0 u 1:2 title @KEY_TITLE1 w @LP_STY1 ,\
     "big-dataset-train-size-vs-time.dat" index 0 u 1:3 title @KEY_TITLE2 w @LP_STY2 , \
     "big-dataset-train-size-vs-time.dat" index 0 u 1:4 title @KEY_TITLE3 w @LP_STY3, \
     "big-dataset-train-size-vs-time.dat" index 0 u 1:5 title @KEY_TITLE4 w @LP_STY4

set origin P2_OX, P2_OY
set size P2_SX, P2_SY
set key off
set ylabel "average time (sec.)" 
set xlabel "window size (samples)"

set title '(b) car-data' 
plot "big-dataset-train-size-vs-time.dat" index 1 u 1:2 title @KEY_TITLE1 w @LP_STY1 , \
     "big-dataset-train-size-vs-time.dat" index 1 u 1:3 title @KEY_TITLE2 w @LP_STY2 , \
     "big-dataset-train-size-vs-time.dat" index 1 u 1:4 title @KEY_TITLE3 w @LP_STY3 , \
     "big-dataset-train-size-vs-time.dat" index 1 u 1:5 title @KEY_TITLE4 w @LP_STY4

#
# Make the outer legend when the key lengths are random and long
#
reset; unset xtics; unset ytics; unset border;

KEY_TOPOFFSET = 0.15

KEY_ORIGX1 = 0.1
KEY_ORIGY1 = 0.05

KEY_SIZEX1 = 0.4
KEY_SIZEY1 = (P1_SY + KEY_TOPOFFSET)

set origin KEY_ORIGX1, KEY_ORIGY1 
set size KEY_SIZEX1, KEY_SIZEY1 
set key center top horizontal Left reverse samplen 2

set title ''

plot [][0:1] 2 title @KEY_TITLE1 w @LP_STY1 ,\
             2 title @KEY_TITLE2 w @LP_STY2

reset; unset xtics; unset ytics; unset border;

KEY_ORIGX2 = KEY_SIZEX1
KEY_ORIGY2 = 0.05

KEY_SIZEX2 = 1-KEY_SIZEX1
KEY_SIZEY2 = (P1_SY + KEY_TOPOFFSET)

set origin KEY_ORIGX2, KEY_ORIGY2
set size KEY_SIZEX2, KEY_SIZEY2

set key center top horizontal Left reverse samplen 2

set title ''

plot [][0:1] 2 title @KEY_TITLE3 w @LP_STY3, \
             2 title @KEY_TITLE4 w @LP_STY4


unset multiplot
