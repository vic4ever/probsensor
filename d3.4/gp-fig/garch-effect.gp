# Turn on macros
set macros

# Load diffrent line chart styles
load "gnuplot-styles/plot_lp_style_1X2.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

## Set font for title
set xrange [1:8]
set yrange [0:30]

set xtics 1
set ytics 5

set multiplot 

set origin P1_OX, P1_OY
set size P1_SX, P1_SY
set key off
set ylabel "Statistic"
set xlabel "m"

KEY_TITLE1 = "'{/TimesItalicBold=19 {/Symbol-Italic=F}(m)}                      '"
KEY_TITLE2 = "'{/TimesItalicBold=19 {/Symbol-Italic=c}^2_m({/Symbol=a})}'"

set title '(a) campus-data'  
plot "garch-effect.dat" index 0 u 1:2 title @KEY_TITLE1 w @LP_STY4 ,\
     "garch-effect.dat" index 0 u 1:3 title @KEY_TITLE2 w @LP_STY2


set origin P2_OX, P2_OY
set size P2_SX, P2_SY
set key off
set ylabel "Statistic" 
set xlabel "m"

set title '(b) car-data' 
plot "garch-effect.dat" index 1 u 1:2 title @KEY_TITLE1 w @LP_STY4 , \
     "garch-effect.dat" index 1 u 1:3 title @KEY_TITLE2 w @LP_STY2


#
# Make the outer legend when the key lengths are random and long
#
reset; unset xtics; unset ytics; unset border;

KEY_TOPOFFSET = 0.15

KEY_ORIGX1 = 0.1
KEY_ORIGY1 = 0.05

KEY_SIZEX1 = 0.9
KEY_SIZEY1 = (P1_SY + KEY_TOPOFFSET)

set origin KEY_ORIGX1, KEY_ORIGY1 
set size KEY_SIZEX1, KEY_SIZEY1 
set key center top horizontal Left reverse samplen 4

set title ''

plot [][0:1] 2 title @KEY_TITLE1 w @LP_STY4 ,\
             2 title @KEY_TITLE2 w @LP_STY2

             
unset multiplot
