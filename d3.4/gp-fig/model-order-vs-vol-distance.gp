# Turn on macros
set macros

# Load diffrent line chart styles
load "gnuplot-styles/plot_lp_style_1X1.gp"

# file name without extension. the final extension is pdf
OP_FILE = '/tmp/plot.eps'

set output OP_FILE

## Set font for title
set xrange [2:8]
set yrange [0.0:3.0]

set xtics 2

set xlabel "model order"
set ylabel "density distance"

set key center top Left reverse samplen 2 spacing 3

KEY_TITLE1 = "'{/TimesBold=20 UT}'"
KEY_TITLE2 = "'{/TimesBold=20 VT}'"
KEY_TITLE3 = "'{/TimesBold=20 ARMA-GARCH}'"

plot "model-order-vs-vol-distance.dat" index 0 u 1:2 title @KEY_TITLE1 w @LP_STY1 ,\
     "model-order-vs-vol-distance.dat" index 0 u 1:3 title @KEY_TITLE2 w @LP_STY2 , \
     "model-order-vs-vol-distance.dat" index 0 u 1:4 title @KEY_TITLE3 w @LP_STY3

