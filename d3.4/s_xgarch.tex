\pagebreak
\subsection{Enhanced GARCH Metric}\label{sec:xgarch}
In practice, time series often contain values that are erroneous in nature. For example, sensor networks, like weather monitoring
stations, frequently produce erroneous values due to various reasons; such as loss of communication, sensor failures,
etc. Unfortunately, the \garch~model is incapable of functioning appropriately when input streams contain such erroneous
values. This is because the \garch~model has been generally used over precise, certain, and clean data (e.g., stock market
data). To tackle this problem, we propose an enhancement of the \garch~metric, which renders the \garch~metric robust against
erroneous time-series inputs.

%This enhancement enables the \garch~metric to handle erroneous values present in the input data in an online fashion.
%Thus the enhanced \garch~model allows us to fully benefit from the \garch~model and at the same time handle erroneous values.

%We start by showing that the \garch~model behaves very differently in the
%presence of erroneous values.
%This difference in behavior is drastic, rendering the \garch~model unusable.
%We examine this behavior since most often data generated by
%sensor networks (for ex., weather monitoring station) contain erroneous values due to reasons like, loss of communication, sensor failures, etc.

Before proceeding further, we note the difference between \emph{erroneous values} and \emph{imprecise values}. Imprecise values
have an inherent element of uncertainty but still follow a particular trend, while erroneous values are significant outliers which
exhibit large unnatural deviations from the trend.

%Moreover, the plain \armagarch~metric is capable of handling imprecise values, but is thrown off-track when erroneous values are intermittently present in the data.

%% \begin{figure}[!h]
%%     \centering
%%     \includegraphics[width=1.0\columnwidth]{fig/garch_with_outliers}
%%     \caption{Behavior of \garch~model when estimation window contains erroneous values.}
%%     \label{fig:garch-outliers}
%% \end{figure}

To give an idea of the change in behavior exhibited by the \garch~model we run the \armagarch~algorithm on all sliding
windows $S^H_{t-1}$ of a time series $S = \langle r_1, r_2 , \ldots, r_{t_m}\rangle$ where $H +1 \leq t \leq t_{m}$ and
$\kappa=3$. The result of executing this algorithm is shown in \figref{fig:xgarch-cleaning}($a$) along with the upper and lower bounds.
%As the \armagarch~algorithm uses a \garch~model, it behaves very differently when erroneous values are present in the window
%$S^H_{t-1}$.
Notice that at time $127$, when the first erroneous value occurs in the training window, the \garch~model
infers an extremely high volatility for the following time steps. This mainly happens since the
\garch~equation \eqnref{eqn:garch-sigma} contains square terms, which significantly amplifies the effect of the presence of erroneous values. To
avoid this we introduce novel heuristics which can be applied to input data in an online fashion and thus obtain a correct
volatility estimate even in the presence of erroneous values. We term our approach \linebreak\ourgarch~(an acronym for Clean-GARCH).

\subsubsection{C-GARCH Model}
Let $S = \langle r_1, r_2 , \ldots, r_{t_m}\rangle$ be a time series containing some erroneous values. We then start executing the \armagarch~procedure (see \algoref{alg:arma-garch}) at time $t > H$. For this we set $\kappa=3$, thus making the probability of finding $r_t$ outside the interval
defined by $u_b$ and $l_b$ low. When we find that $r_{t}$ resides outside $u_b$ and $l_b$, we mark it as erroneous value and
replace it with the corresponding inferred value $\hat{r}_{t}$. Simultaneously, we also keep the track of the number of consecutive
values we have marked as erroneous values most recently. If this number exceeds a predefined constant $o_{c_{max}}$ then we assume that
the observed raw values are exhibiting a changing trend. For example, during sunrise the ambient temperature exhibits a rapid change
of trend.
%
%% \begin{algorithm}[hbt]
%% \caption{\bf Online cleaning of erroneous values using \ourgarch~model (\emph{CleanGARCH}).} \label{alg:ourgarch}
%% \begin{algorithmic}[1]
%%      \INPUT Time series $\langle r_1, \ldots, r_{t_m}\rangle$, model parameters $(p,q)$ for \arma~model, window size $H$,
%%      $\kappa$, $SV_{max}$, and $o_{c_{max}}$
%%      \OUTPUT  Upper bound $u_b$, lower bound $l_b$, all the inferred values of $\hat{r}_{t}$ and $\hat{\sigma}^2_{t}$
%%      \State $t \leftarrow H + max(p,q)$
%%          \While{$t \leq t_m$}
%%             \State $(\hat{r}_{t}, \hat{\sigma}^2_{t}, u_b, l_b) = ARMA-GARCH(S^H_{t-1}, \kappa, p , q)$
%%             \If{$r_t \geq u_b$ \textbf{or} $r_t \leq l_b$}
%%                 \State $r_t \leftarrow \hat{r}_{t}$
%%                 \State $o_c \leftarrow o_c + 1$
%%             \Else
%%                 \State $o_c \leftarrow 0$
%%             \EndIf
%%             \If{$o_c \geq o_{c_{max}}$}
%%                 \State $[r_{t-o_{c_{max}}}, r_t] \leftarrow SVRFilt([r_{t-o_{c_{max}}}, r_t], SV_{max})$
%%                 \State $o_c \leftarrow 0$
%%             \EndIf
%%             \State $t \leftarrow t+1$
%%          \EndWhile
%% \end{algorithmic}
%% \end{algorithm}
%% Intuitively, if consecutive $o_{c_{max}}$ values are marked as erroneous, it means that there is a change of trend occurring in
%% the raw values.
This idea inherently assumes that the probability of finding $o_{c_{max}}$ consecutive erroneous values is
low. And, if we find $o_{c_{max}}$ consecutive erroneous values we should re-adjust the model to the new trend.

Although it rarely happens in practice that there are many consecutive erroneous values may be present in raw data. To rule out the
possibility of using these values for inference, we introduce a novel heuristic that is applied to the values in the window
$[r_{t-o_{c_{max}}}, \ldots, r_t]$ before they are used for the inference. This step ensures that we have not included any erroneous
values present in the raw data into our system. Thus we avoid the problems that occur by using a simple
\armagarch~metric.
%
\begin{figure}[!t]
\centering
\subfloat[]{ \includegraphics[width=0.492\columnwidth]{fig/garch_with_outliers.pdf}}  \hfil
\subfloat[]{\includegraphics[width=0.492\columnwidth]{fig/xgarch_cleaning.pdf}} \hfil
\qquad
\caption{(a) Behavior of the \garch~model when window $S^H_{t-1}$ contains erroneous values. (b) Result of using the \ourgarch~model.}
\label{fig:xgarch-cleaning}
\end{figure}
%
\subsubsection{Successive Variance Reduction Filter}
The heuristic that we use for filtering out significant anomalies is shown in \algoref{alg:vari-red}. This algorithm takes values
$\mathcal{V}=[v_1, v_2, \ldots, v_K]$ containing erroneous values and a thresholding parameter $SV_{max}$ as input. It first
measures dispersion of $\mathcal{V}$ by computing its sample variance denoted as $SV(\mathcal{V})$ (Step 3). Then we delete a
point, say $v_k$, and compute the sample variance of all the other points $[v_1, \ldots, v_{k-1}, v_{k+1}, \ldots, v_K]$ denoted
as $SV(\mathcal{V}\backslash v_k)$ (Step 9). We perform this procedure for all points and then finally find a value $v_{\bar{k}}$
such that this value, if deleted, gives us the maximum variance reduction. We delete this point and reconstruct a new value at
$\bar{k}$ using interpolation. We stop this procedure when the total sample variance becomes less than the variance threshold
$SV_{max}$. In Steps 8 and 9, we use the intermediate values $\hat{v}'_K$ and $\hat{v}_{K}$ to compute $SV(\mathcal{V}\backslash
v_K)$, thus reducing the computational complexity of the algorithm to quadratic.

\begin{figure}[!h]
    \centering
    \includegraphics[width=1\columnwidth]{fig/varired_example_small}
    \caption{Showing sample run of the Successive Variance Reduction Filter (\algoref{alg:vari-red}).}
    \label{fig:varired-example}
\end{figure}

A graphical example of our approach is shown in \figref{fig:varired-example}. From this figure we can see that values at $k_1$ and
$k_2$ are erroneous. In the first iteration our algorithm deletes value $v_{k_1}$ and reconstructs it.  Next, we delete $v_{k_2}$ and
obtain a new value using interpolation. At this point we stop since $SV(\mathcal{V})$ becomes less than $SV_{max}$. Moreover, it
is very important to know a fair value for $SV_{max}$, since if a higher value is chosen we might include some erroneous values
and if a lower value is chosen we might delete some non-erroneous values. Also, the value of $SV_{max}$ depends on the underlying
parameter monitored. For example, ambient temperature in \figref{fig:vol-mo-comp} shows rapid changes in trend as compared to
relative humidity.  Thus, using a sample of size $T$ of clean data, we compute $SV_{max}$ as the maximum sample variance
(dispersion) we observe in all sliding windows of size $o_{c_{max}}$. This gives a fair estimate of the threshold between trend
changes and erroneous values.

%% Our algorithm deletes the point which when not considered reduces the sample variance the most. Thus
%% value $v_{k_1}$ is chosen, other values, like $v_{k_2}$ are not choosen for deleting in the first iteration. This happens since if
%% we choose $v_{k_2}$ in the first iteration then our dataset includes $v_{k_1}$ which pushes the variance higher than obtained from
%% dropping $v_{k_1}$. Next, we delete $v_{k_1}$ and obtain a new value for it using interpolation. We continue these
%% delete-interpolate iterations until $SV(\mathcal{V})$ is not greater than $SV_{max}$. Going back to our toy example in
%% \figref{fig:varired-example}, when we delete two erroneous values ($v_{k_1}$ and $v_{k_2}$) we stop the delete-interpolate
%% iterations.

%% Also, the value of $SV_{max}$ depends on the underlying parameter monitored. For example, \figref{fig:outlier-param-exs} shows two
%% parameters wind velocity and ambient temperature. Clearly, wind velocity exhibits rapid change of trend while ambient temperature
%% does not exhibit rapid trend changes. Thus $SV_{max}$ should be higher for wind velocity and lower for ambient temperature.
%% Moreover, for a sample of size $T$ of clean data, we define $SV_{max}$ as the maximum sample variance (dispersion) we observe in
%% all sliding windows of size $o_{c_{max}}$. With this, $SV_{max}$ for wind velocity is $53.31$ and for ambient temperature
%% $SV_{max} = 0.57$. This gives a rough estimate of the threshold difference between trend changes and erroneous
%% values. Furthermore, setting $o_{c_{max}}$ requires domain knowledge about sensors used for data gathering. For example, if there
%% are unreliable sensors which frequently emit erroneous values then setting a higher value for $o_{c_{max}}$ is advisable and vice
%% versa.

%% \Begin{algorithm}[hbt]
%% \caption{\bf Learning thresholding parameter $SV_{max}$ (\emph{LearnThresParam}).} \label{alg:learn_vmax}
%% \begin{algorithmic}[1]
%%      \INPUT Values $[v_1, v_2, \ldots, v_K]$, $o_{c_{max}}$
%%      \OUTPUT  $SV_{max}$
%%          %% \State $k \leftarrow o_{c_{max}} + 1$
%%          %% \Repeat
%%          %%    %\If{$Var([v_{k-{o_{c_{max}}}}, v_k]) > V_{max}$}
%%          %%    \State $\vartheta_{k} \leftarrow SV([v_{k-{o_{c_{max}}}}, v_k])$
%%          %%    %\EndIf
%%          %% \Until{$k \leq K$}
%%          %% \State $SV_{max} = \frac{1}{K-o_{c_{max}}} \sum_{i=o_{c_{max}}}^K \vartheta_i$
%%          \State $k \leftarrow o_{c_{max}} + 1$ and $SV_{max} \leftarrow -\infty$
%%          \Repeat
%%             %\If{$Var([v_{k-{o_{c_{max}}}}, v_k]) > V_{max}$}
%%             \If{$SV([v_{k-{o_{c_{max}}}}, v_k]) > SV_{max}$ }
%%             \State $SV_{max} \leftarrow SV([v_{k-{o_{c_{max}}}}, v_k])$
%%             \EndIf
%%          \Until{$k \leq K$}
%%          %\State $SV_{max} = \frac{1}{K-o_{c_{max}}} \sum_{i=o_{c_{max}}}^K \vartheta_i$
%% \end{algorithmic}
%% \end{algorithm}
%% %
%% We learn $SV_{max}$ using clean data of $T$ samples. \algoref{alg:learn_vmax} shows the learning procedure. This algorithm
%% simply computes the maximum sample variance observed in all sliding windows of size $o_{c_{max}}$ and assigns this value to
%% $SV_{max}$. Intuitively, $SV_{max}$ records the amount of maximum dispersion that we see in all sliding windows of size
%% $o_{c_{max}}$, thus if the data is inherently noisy or contains rapid trend changes then we expect to obtain a higher value for
%% $SV_{max}$. On the contrary, if the data does not exhibit rapid trend changes $SV_{max}$ would be low. This gives a rough estimate
%% of the threshold difference between trend changes and erroneous values.
%
%% \begin{figure}
%% \centering
%% \subfloat[]{ \includegraphics[width=0.48\columnwidth]{fig/outlier_param_ex1.pdf}}  \hfil
%% \subfloat[]{\includegraphics[width=0.48\columnwidth]{fig/outlier_param_ex2.pdf}} \hfil
%% \qquad
%% \caption{Example of trend changes in (a) ambient temperature data and (b) wind direction data.}
%% \label{fig:outlier-param-exs}
%% \end{figure}


%
\figref{fig:xgarch-cleaning}($b$) shows the result of using \ourgarch~model on the same data as shown in \figref{fig:xgarch-cleaning}($a$)
with $o_{c_{max}} = 7$. We can observe that at $t=93$ a trend change starts to occur and is smoothly corrected by the
\ourgarch~model at $t=101$. Most importantly, the successive variance reduction filter effectively handles the erroneous values
occurring at times $t=127$ and $t=132$. Thus the \ourgarch~model performs as expected and overcomes the shortcomings of the plain
\armagarch~metric. In \secref{sec:exp} we will demonstrate the efficacy of the \ourgarch~model on real data obtained from sensor networks.

%% \begin{figure}[!h]
%%     \centering
%%     \includegraphics[width=1.0\columnwidth]{fig/xgarch_cleaning}
%%     \caption{Result of applying the \ourgarch~model.}
%%     \label{fig:xgarch-cleaning}
%% \end{figure}
\begin{algorithm}[hbt]
\small
\caption{The Successive Variance Reduction Filter.} \label{alg:vari-red}
\begin{algorithmic}[1]
     \INPUT A time series $\mathcal{V}$ containing erroneous values and variance threshold $SV_{max}$.
     \OUTPUT  Cleaned values $\mathcal{V}$.
         \While{true}
             \State $\hat{v}'_K \leftarrow \sum_{k=1}^K v_k^2$ and $\hat{v}_{K} \leftarrow \frac{1}{K}\sum_{k=1}^K v_k$
             \State $SV(\mathcal{V}) \leftarrow \frac{1}{K-1}\hat{v}'_K - \frac{K}{K-1} (\hat{v}_K)^2$
             \If{$SV(\mathcal{V}) > SV_{max}$}
                   \State \textbf{break}
             \EndIf
             \State $cVar \leftarrow -\infty$, $\bar{k} \leftarrow 0$, and $k \leftarrow 1$
             \Repeat
                \State $\hat{v}'_{K-1} \leftarrow \hat{v}'_K - v_k^2$ and $\hat{v}_{K-1} \leftarrow \hat{v}_K - \frac{v_k}{K}$
                \State $SV(\mathcal{V} \backslash v_k) \leftarrow \frac{1}{K-2}\hat{v}_{K-1} - \frac{K-1}{K-2} (\hat{v}_{K-1})^2$
                \If{$SV(\mathcal{V} \backslash v_k) < cVar $}
                   \State $cVar \leftarrow SV(\mathcal{V} \backslash v_k)$
                   \State $\bar{k} \leftarrow k$
                \EndIf
                \State $k \leftarrow k+1$
             \Until{$k \leq K$}
             \State Mark $v_{\bar{k}}$ as erroneous and delete
             \If{$\bar{k} \neq 1$ \textbf{and} $\bar{k} \neq K$}
                \State Use $v_{\bar{k} -1}$ and $v_{\bar{k} +1}$ to interpolate the value of $v_{\bar{k}}$
             \Else
                \State Extrapolate $v_{\bar{k}}$
             \EndIf
         \EndWhile
\end{algorithmic}
\end{algorithm}

\sstitle{Guidelines for Parameter Setting} The \ourgarch~model requires three parameters $\kappa$, $SV_{max}$, and
$o_{c_{max}}$. In most cases we assign $\kappa=3$. As seen before, $SV_{max}$ is learned from a sample of clean data. On the
contrary, setting $o_{c_{max}}$ requires domain knowledge about sensors used for data gathering. If there are unreliable sensors
which frequently emit erroneous values then setting a higher value for $o_{c_{max}}$ is advisable and vice versa. Our
experiments suggest that the \ourgarch~model performs satisfactorily when the value for $o_{c_{max}}$ is set to twice the length of the longest sequence of erroneous values. In practice, $o_{c_{max}}$ is generally small, making the execution of \algoref{alg:vari-red} efficient.

