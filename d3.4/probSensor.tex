\documentclass[10pt,conference,letterpaper]{IEEEtran}
\usepackage{amsmath,epsfig,amsfonts}
\usepackage{times}
\usepackage{marvosym}
\usepackage{url}
\usepackage{array}
\usepackage{comment}
\usepackage{algorithmicx}
\usepackage[noend]{algpseudocode}
\usepackage{algorithm}
\usepackage{multirow}
\usepackage{mathrsfs}
\usepackage[caption=false]{subfig}
\usepackage{cite}
\usepackage{mathtools}
\usepackage{bbold}
\usepackage{cleveref}
\usepackage{booktabs}
\usepackage{datetime}
\usepackage{dblfloatfix}
\usepackage{color}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{amssymb}
%\usepackage[linesnumbered, ruled, vlined]{algorithm2e}
%\usepackage{appendix}
%\usepackage[mathcal]{euscript}

\newcommand{\stitle}[1]{\vspace*{0.5em}\noindent{\bf #1.\/}$\\$ }
\newcommand{\sstitle}[1]{\vspace*{0.4em}\noindent{\bf #1:\/}}
\newcommand{\todo}{\vspace*{0.4em}\noindent{\bf [To be rewritten] }}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}
\newtheorem{observation}{Observation}
\newtheorem{theorem}{Theorem}

\newcommand{\algorithmicinput}{\textbf{Input:}}
\newcommand{\algorithmicoutput}{\textbf{Output:}}
\newcommand{\algorithmicdescription}{\textbf{Description:}}
\newcommand{\algorithmicbreak}{\textbf{break}}
\newcommand{\algorithmiccontinue}{\textbf{continue}}
%\newcommand{\algorithmicreturn}{\textbf{return~}}
\newcommand{\INPUT}{\item[{\algorithmicinput}]$\phantom{1}$}
\newcommand{\OUTPUT}{\item[\algorithmicoutput]$\phantom{1}$}
\newcommand{\DESCRIPTION}{\item[\algorithmicdescription]$\phantom{1}$\\}
\newcommand{\BREAK}{\State{\algorithmicbreak}}
\newcommand{\CONTINUE}{\State{\algorithmiccontinue}}
\newcommand{\RETURN}{\State{\algorithmicreturn}~}

\newcommand{\etal}{\emph{et al.}}
\newcommand{\secref}[1]{Section~\ref{#1}}
\newcommand{\apdxref}[1]{Appendix~\ref{#1}}
%\newcommand{\figref}[1]{Figure~\ref{#1}}
\newcommand{\figref}[1]{Fig.~\ref{#1}}
\newcommand{\defref}[1]{Definition~\ref{#1}}
\newcommand{\lemref}[1]{Lemma~\ref{#1}}
\newcommand{\thmref}[1]{Theorem~\ref{#1}}
\newcommand{\obsref}[1]{Observation~\ref{#1}}
\newcommand{\wrt}{\emph{w.r.t.}}

\newcommand{\tabref}[1]{Table~\ref{#1}}
\newcommand{\eqnref}[1]{(\ref{#1})}
\newcommand{\algoref}[1]{Algorithm~\ref{#1}}
\newcommand{\fixme}[1]{(\textbf{\IroningII~\emph{#1}})}
\newcommand{\bldm}[1]{\mbox{\boldmath$#1$}}
\newcommand{\scr}[1]{\mathscr{#1}}
%\newcommand{\ourgarch}{\emph{C-GARCH}}
%\newcommand{\garch}{\emph{GARCH}}
%\newcommand{\arma}{\emph{ARMA}}
\newcommand{\ourgarch}{C-GARCH}
\newcommand{\garch}{GARCH}
\newcommand{\arma}{ARMA}

\newcommand{\armagarch}{ARMA-GARCH}
\newcommand{\kalgarch}{Kalman-GARCH}
\newcommand{\bb}[1]{\mathbb{#1}}

\newtheorem{mydef}{Definition}
\newtheorem{mythm}{Theorem}
\newtheorem{myprob}{Problem}
\newtheorem{mystat}{PROBLEM STATEMENT}
\newtheorem{scenario}{SCENARIO}
\newtheorem{example}{Example}
\def\nas{\ensuremath{\mathrm{not}}}
\newcommand{\mi}[1]{\ensuremath{\mathit{#1}}}

\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\myurl}[1]{{{\scriptsize\url{#1}}}}

\setlength{\textfloatsep}{2ex} \setlength{\intextsep}{1.2ex}
\setlength{\dbltextfloatsep}{2ex} \addtolength{\topskip}{-3.0mm}
\setlength{\textheight}{9.33in}

\IEEEoverridecommandlockouts

\begin{document}

\title{Creating Probabilistic Databases from Imprecise Time-Series Data}
\author{%
{Saket Sathe, Hoyoung Jeung, Karl Aberer}
\vspace{1.6mm}\\
\fontsize{10}{10}\selectfont\itshape
%### below can be removed
%School of Computer and Communication Sciences\\
Ecole Polytechnique F\'ed\'erale de Lausanne (EPFL), Switzerland\\
\fontsize{9}{9}\selectfont\ttfamily\upshape
\{saket.sathe, \vspace{0.2cm} hoyoung.jeung, \vspace{0.2cm} karl.aberer\}@epfl.ch
\thanks{The work presented here was supported by the National
Competence Center in Research on Mobile Information and
Communication Systems (NCCR-MICS), a center supported
by the Swiss National Science Foundation under grant number 5005-67322.
}
}

\maketitle

\begin{abstract}
%Although efficient processing of probabilistic databases is a well-established field, a wide range of applications are still unable to benefit from these techniques due to the lack of means for creating probabilistic databases. In fact, it is a challenging problem to associate concrete probability values with given time-series data for forming a probabilistic database,
%Prior work on this problem has only limited scope for domain-specific applications. The problem becomes even more difficult for time-series data,
%since the probability distributions used for deriving such probability values vary over time.
%
%In this paper, we propose a novel approach to create tuple-level probabilistic databases from (imprecise) time-series data.
%To the best of our knowledge, this is the first work that introduces a generic solution for creating probabilistic databases from arbitrary time series, which can work in online as well as offline fashion.
%Our proposal is designed to work as an independent module, so that it can be easily embedded into existing data management systems dealing with on-line as well as off-line processing.
%Our approach consists of two key components. First, the {\em dynamic density metrics} that infer time-dependent probability distributions for time series, based on various mathematical models. Our main metric, called the GARCH metric, can robustly capture such evolving probability distributions
%regardless of the presence of erroneous values in a given time series.
%
%Second, the {\em $\Omega$--View builder} that creates probabilistic databases from the probability distributions inferred by the dynamic density metrics. For efficient processing, we introduce the $\sigma$--cache that reuses the information derived from probability values generated at previous times.
%
%Extensive experiments over real datasets demonstrate the effectiveness of our approach.
\end{abstract}

\tableofcontents

\input{note}
\input{s_intro_2}
%\input{s_prob}
\input{single}

\input{multi}

%\input{s_query}
\input{s_exp}
%\input{s_related}
%\input{s_con}

\vspace{1.6cm}
\bibliographystyle{IEEEtran}
\pagebreak
%\bibliography{../bibtex/stream}
%\input{s_appendix}

\end{document}
