\section{GARCH Metric}\label{sec:garch}

As stated in the previous subsection, it is common to capture the uncertainty of an imprecise time series with a fixed-size
uncertainty range as shown in \figref{fig:naive}($a$) \cite{cheng03,Cheng2004}. This approach, however, may not be effective in
practice, since in a wide variety of real-world settings, the size of the uncertainty range typically varies over time. For
example, \figref{fig:vol-mo-comp} shows two time series obtained from a real sensor network deployment monitoring ambient
temperature and relative humidity. The regions marked as \emph{Region A} in \figref{fig:vol-mo-comp}($a$) and \figref{fig:vol-mo-comp}($b$)
exhibit higher volatility\footnote{We use \emph{variance} and \emph{volatility} interchangeably.} than those marked as
\emph{Region B}. This observation strongly suggests that the underlying model should support time-varying variance and mean value
when it infers a probability density function. We experimentally verify this claim in \secref{subsec:verify-tvol}.

Motivated by this, we introduce a new dynamic density metric, the \emph{\garch~metric}. The \garch~metric models $p_t(R_t)$ as a
Gaussian probability density function $\mathcal{N}(\hat{r}_t,\hat{\sigma}_t^2)$. This metric assumes that the underlying time series
exhibits not only time-varying average behavior ($\hat{r}_t$) but also time-varying variance ($\hat{\sigma}^2_t$). For inferring
$\hat{\sigma}^2_t$ we propose using the \garch~model. And, for inferring $\hat{r}_t$ we can either use the \arma~model from
\secref{sec:metrics} or Kalman Filters.

%% Naturally, an important question is whether real world data shows regimes of changing volatility \footnote{We use \emph{variance} and \emph{volatility}
%% interchangeably.}. To address this question we show in \figref{fig:vol-mo-comp} examples of time series from
%% a real sensor network deployment monitoring ambient temperature and humidity. The regions shown as \emph{Region A} in \figref{fig:vol-mo-comp}($a$) and
%% \figref{fig:vol-mo-comp}($b$) exhibit higher volatility than \emph{Region B}. This observation strongly suggests that the
%% underlying model should support a time-varying variance since real sensor network data
%% exhibits regimes of changing volatility. Moreover, this clearly contradicts the assumption made by previous works
%% that variance is fixed over time \cite{cheng03,Cheng2004}. In the following paragraphs we will introduce the \garch~model, which
%% accurately models the time-varying variance.

\begin{figure}
\centering
\subfloat[]{ \includegraphics[width=0.486\columnwidth]{fig/example_vol_regions.pdf}}  \hfil
\subfloat[]{\includegraphics[width=0.486\columnwidth]{fig/example_vol_regions-rh.pdf}} \hfil
\qquad
\caption{Regions of changing volatility in (a) ambient temperature and (b) relative humidity.}
\label{fig:vol-mo-comp}
\end{figure}

\section{The GARCH Model}
The \garch~(\underline{G}eneralized \underline{A}utoRegressive \underline{C}onditional
\underline{H}eteroskedasticity) model \cite{shumway2005} efficiently captures time-varying volatility in a time series.
%% It assumes
%% that the underlying time-series follows a conditionally Gaussian distribution at each time step. Volatility modelling in time-series
%% was first studied by Engle~\cite{engle1982}. The model proposed in \cite{engle1982} was simple autoregressive model which was
%% known as ARCH (AutoRegressive Conditional Heteroscedasticity) model. Later this model was extended by Bollerslev
%% \cite{bollerslev1986} to a generalized form known as the \garch~(Generalized AutoRegressive Conditional Heteroscedasticity)
%% model.
%%
%%
%% \figref{fig:sum_models} summarizes the models which we use for modelling conditional mean and volatility of sensor data. As
%% shown in the figure we use an \arma~model or Kalman Filter for modelling mean behavior and we use \garch~model for modelling
%% volatility. In the following paragraphs we describe Kalman Filter and \garch~model and discuss their use for estimating and predicting fidelity
%% information.
%%
%% \begin{figure}[htb]
%%   \begin{center}
%%     \hspace{-0.5cm}\includegraphics[width=0.8\columnwidth]{fig/models_summary}
%%     \caption{Summary of uncertainty models.}
%%     \label{fig:sum_models}
%%     \end{center}
%%  \end{figure}
%%
%% Let us denote the sensor value at time $t$ as $r_t$. Also, let the conditional mean value at time $t$ be denoted as $\mu_t$ and the conditional variance be denoted as $\sigma^2_t$. Formally,
%% \begin{equation}
%% \label{eqn:gen-models}
%% \mu_t = E(r_t | F_{t-1}) \quad \sigma^2_t = E[(r_t - \mu_t)^2 | F_{t-1}],
%% \end{equation}
%% where $E(\cdot)$ is the standard expectation operator and $F_{t-1}$ denotes all the information available at time $t-1$, which in our case includes sensor values form a previous time window of size $H$ denoted as $[r_{t-H},r_t]$.
%%
%% The \arma~model models $\mu_t$ using a simple linear stationary model. The \arma~model simply models the time-varying mean value as,
%% %% Moreover, $\mu_t$ is modelled using a simple linear stationary model, like, the $ARMA(p,q)$. The $ARMA(p,q)$ model simply models the time-varying mean value as,
%% \begin{equation}
%% \label{eqn:spe-models}
%% r_t = \mu_t + a_t, \quad \mu_t = \phi_o + \sum_{i=1}^p \phi_i r_{t-i} - \sum_{i=1}^q \theta_i a_{t-i},
%% \end{equation}
%% where $a_t$ obeys a zero mean normal distribution with variance $\sigma^2_a$, $\phi_1, \ldots, \phi_p$ are autoregressive coefficients, $\theta_1, \ldots, \theta_q$ are moving average coefficients, and $(p,q)$ are non-negative integers denoting the model order. Generally, an \arma~model with model order $(p,q)$ is denoted as $ARMA(p,q)$.
Specifically, given a window $S_{t-1}^H$, the \arma~model models $r_i = \hat{r}_i + a_i$ where $t-H \leq i \leq t-1$.

We then define the conditional variance $\sigma_i^2$ as:
\begin{equation}
\label{eqn:vol-model}
\sigma^2_i = \bb{E}((r_i - \hat{r}_i)^2 | F_{i-1}), \quad \sigma^2_i = \bb{E}(a_i^2 | F_{i-1}),
\end{equation}
where $\bb{E}(a_i^2 | F_{i-1})$ is the variance of $a_i$ given all the information $F_{i-1}$ available until time $i-1$.
%% Next, we combine \eqnref{eqn:gen-models} and \eqnref{eqn:spe-models} to get,
%% \begin{equation}
%% \label{eqn:vol-model}
%% \sigma_t^2 = Var(r_t | F_{t-1}) = Var(a_t | F_{t-1}),
%% \end{equation}
%% where $Var(\cdot)$ is the standard variance operator.
The GARCH(m,s) model models volatility in \eqnref{eqn:vol-model} as a linear function of $a^2_i$ as:
\begin{equation}
\label{eqn:garch-sigma}
a_i = \sigma_i \epsilon_i, \quad \sigma_i^2 = \alpha_0 + \sum_{j=1}^{m} \alpha_ja_{i-j}^2 + \sum_{j=1}^{s} \beta_j \sigma^2_{i-j},
\end{equation}
where $\epsilon_i$ is a sequence of independent and identically distributed (\emph{i.i.d}) random variables, $(m,s)$ are
parameters describing the model order, $\alpha_0 > 0$, $\alpha_j \geq 0$, $\beta_j \geq 0$, $\sum_{j=1}^{max(m,s)}
(\alpha_j + \beta_j) < 1$, and $i$ takes values between $t-H + max(m,s)$ and $t -1$.

The underlying idea of the GARCH(m,s) model is to reflect the fact that
large shocks ($a_i$) tend to be followed by other large shocks. Unlike the $s^2_t$ in the variable thresholding metric, $\sigma^2_i$ is a variance that is estimated after subtracting the local trend $\hat{r}_i$. In many practical applications the
\garch~model is typically used as the GARCH(1,1) model, since for a higher order \garch~model specifying the model order is a difficult task
\cite{shumway2005}. Thus, we restrict ourselves to these model order settings. More details regarding the estimation of model parameters and the choice for the sliding window size $H$ are described in \cite{shumway2005}.
%% In \secref{sec:exp} we experimentally show that real datasets show these characteristics and thus the \garch~model could be efficiently and effectively used for obtaining fidelity information.

For inferring time-varying volatility, we use the GARCH(m,s) model and $a_i$ as follows:
\begin{equation}
\hat{\sigma}_{t}^2 = \alpha_0 + \sum_{j=1}^{m} \alpha_j a_{t-j}^2 + \sum_{j=1}^{s} \beta_j \sigma_{t-j}^2.
\end{equation}
%
Recall that we use the \arma~model for inferring the value of $\hat{r}_t$ given $S_{t-1}^H$. We also consider the Kalman Filter \cite{shumway2005} for inferring
$\hat{r}_t$. We show the difference in performance between the Kalman Filter and the \arma~model in \secref{subsec:ddm-comp}.
Basically, the Kalman Filter models $\hat{r}_t$ using the following two equations,
\begin{align}
&\text{state equation:} \; \hat{r}_{i} = c_1 \cdot \hat{r}_{i-1} + e_{i-1}   \; \; \; \;  e_i \sim \mathcal{N}(0,\sigma_e^2) \label{eqn:kalman-state},\\
&\text{observation equation:} \; r_{i} =  c_2 \cdot \hat{r}_{i} + \eta_{i}  \; \; \; \; \eta_i \sim \mathcal{N}(0,\sigma_{\eta}^2) \label{eqn:kalman-obs},
\end{align}
where $\hat{r}_{1}$ is given a priori and $c_1$ and $c_2$ are constants. Since the \garch~model in \eqnref{eqn:garch-sigma} takes errors $a_i$ as input,
they are computed as $a_i = r_i - \hat{r}_i$ and are used by the \garch~model.

\vspace{0.2cm}
Considering both approaches for inferring $\hat{r}_t$ (\arma~model and Kalman Filter) we propose two dynamic density metrics,
namely, \emph{\armagarch}~and \emph{\kalgarch}. Both of them use the \garch~model for inferring $\hat{\sigma}_t$.  But for inferring $\hat{r}_t$ they use
\arma~model and Kalman Filter respectively.

%\textbf{TODO: them algorithm vao laiiiiiiiiiiiiiiiii}
%
\begin{algorithm}[!h]
\small
%\footnotesize
\caption{Inferring $\hat{r}_{t}$ and $\hat{\sigma}^2_{t}$ using \armagarch.} \label{alg:arma-garch}
\begin{algorithmic}[1]
     \INPUT \arma~model parameters $(p,q)$, sliding window $S^H_{t-1}$, and scaling factor $\kappa$.
     \OUTPUT  Inferred $\hat{r}_{t}$, inferred volatility $\hat{\sigma}^2_{t}$, and $\kappa$-scaled bounds $u_b,l_b$.
        \State Estimate an $ARMA(p,q)$ model on  $S^H_{t-1}$ and obtain $a_i$ where $t-H+max(p,q) \leq i \leq t-1$
        \State Estimate a $GARCH(1,1)$ model using $a_i$'s
        \State Infer $\hat{r}_{t}$ using $ARMA(p,q)$ and $\hat{\sigma}^2_{t}$ using $GARCH(1,1)$
        \State $u_b \leftarrow \hat{r}_t + \kappa\hat{\sigma}_{t}$ and $l_b \leftarrow \hat{r}_{t} - \kappa\hat{\sigma}_{t}$
        \RETURN $\hat{r}_{t}$, $\hat{\sigma}^2_{t}$, $u_b$, and $l_b$
\end{algorithmic}
\end{algorithm}

\algoref{alg:arma-garch} gives a concise description of the \armagarch~metric. This algorithm uses the \arma~model for
inferring $\hat{r}_t$ and the \garch~model for inferring $\hat{\sigma}^2_{t}$ (Step 3). The algorithm for \kalgarch~metric is the
same as \algoref{alg:arma-garch}, except that it uses the Kalman filter in Step 3 for inferring $\hat{r}_t$ instead of using the
\arma~model.  Here, $\kappa \geq 0 $ is a scaling factor that decides the upper bound $u_b$ and the lower bound $l_b$. For example,
when $\kappa=3$, the probability that $r_t$ lies between $u_b$ and $l_b$ is very high (approximately 0.9973).

The time complexities of the estimation step for the \arma~model and the \garch~model (Step 1 and 2) are $\mathcal{O}(H\cdot max(p,q))$ and $\mathcal{O}(H\cdot
max(m,s))$ respectively \cite{minka:2003}. Nevertheless, as the model order parameters are small as compared to $H$
these estimation steps become significantly efficient.
%
%% Moreover, $\kappa$ is the probability  that  $(r_{t} \leq u_b)$. $u_b$ and $l_b$ are computed using an inverse cumulative
%% normal distribution denoted as $F^{-1}_{G}(\rho;\mu,\sigma^2)$, where $\mu$ is the mean, $\sigma^2$ is the variance, and $\rho$
%% is probability where $\rho \in (0,1)$. Then the upper bound ($u_b$) and lower bound ($l_b$) are as follows,
%% \begin{equation}
%% u_b = F^{-1}_{G}(\kappa;\hat{r}_{t+1},\sigma^2_{t+1}) \quad l_b = F^{-1}_{G}(1-\kappa;\hat{r}_{t+1},\sigma^2_{t+1}).
%% \end{equation}
%
%% Thus by using $\kappa$ we can make a relative change in the upper and lower bounds produced by the algorithm. This, in turn, decides the probability with which $r_t$ lies between $u_b$ and $l_b$. If $\kappa$  is not specified, it may also be specified at query time to generate bounds if required.

%This completes the discussion of all the dynamic density metrics used in this paper. In the next subsection we study the behavior of
%the \garch~model when the raw input values contain erroneous values.

% Large amounts of sensor data is generated every hour by measurement stations situated at various remote locations. These stations
% consist of many sensors which continuously monitor various parameters of interest and generate relevant data. The generated data
% is then streamed through to database servers and is stored using frameworks like GSN (Global Sensor Networks)
% \cite{aberer06}. Later, domain experts use this data for further analysis. For example, the data obtained from stations monitoring
% atmospheric parameters (like, wind velocity, temperature, atmospheric pressure) is used by environmental scientists to forecast
% and analyse changes in the weather conditions. These weather conditions encompass predicting avalanches, landslides, warming of
% glaciers, or any other abrupt climate changes.

% Moreover, due to certain limitations such as low bandwidth and battery life the sensors might transmit erroneous values. Thus,
% probabilistic models are employed to continuously infer the true sensor value given the erroneous values. In these models the true
% sensor value is considered as a hidden state and the erroneous values are used to continuously infer (or predict) the true value
% in a probabilistic manner. Furthermore, true value is nothing but the most probable value given the complete inferred probability
% density. Thus until the subsequent update arrives the data which the database holds is inherently uncertain. Likewise, for reasons
% of simplicity, we can associate an uncertainty bound with each update. This bound indicates the range in which the true value
% could be found with maximum probability. Now, with the help of this knowledge probabilistic answers to certain SQL queries are
% possible. These queries typically deal with answering requests like, \emph{``what is the lowest temperature value?''}. Answering
% such a query is particularly challenging since all the values in the database are uncertain which renders identifying a single
% lowest value infeasible. Thus a typical answer to this query is a list of values where each value is associated with a probability
% of it being the lowest.

% HY: provide the comparisons of each metric
