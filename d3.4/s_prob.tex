\section{Problem modeling} \label{sec:prob}
This subsection describes our framework, defines queries this study considers, and proposes a measure for quantifying the
effectiveness of the dynamic density metrics. \tabref{tab:notation} offers the notations used in this paper.

\subsection{Our approach}

\figref{fig:framework} illustrates our framework for creating probabilistic databases, consisting of two key components that are dynamic density metrics and the $\Omega$--View builder.
%
A {\em dynamic density metric} is a system of measure that dynamically infers time-dependent probability distributions of imprecise raw values. It takes as input a sliding window that contains recent previous values in the time series.
In the following subsections, we introduce various dynamic density metrics.

\begin{figure}[!h]
    \center
    \includegraphics[width=1.0\columnwidth]{fig/framework}
    \caption{Architecture of the framework.}
    \label{fig:framework}
\end{figure}

Let $S = \langle r_1, r_2,\cdots,r_t \rangle$ be a time series, represented by a sequence of timestamped values, where $r_i \in S$ indicates a (imprecise) raw value at time $i$. Let $S^H_{t-1} = \langle r_{t-H}, r_{t-H+1},$ $\cdots,r_{t-1} \rangle$ be a (sliding) window that is a subsequence of $S$, where its ending value is at the previous time of $t$. The dynamic density metrics correspond to the following query:

\begin{definition} {\bf Inference of dynamic probability distribution.}
    Given a (sliding) window $S^H_{t-1}$, the inference of a probability distribution at time $t$ estimates a probability density function $p_t(R_t)$, where $R_t$ is a random variable associated with $r_t$.
    \label{def:inference}
\end{definition}
The system stores the inferred probability density functions $p_t(R_t)$ associated with the corresponding raw values.
%These distributions can then be used for effortlessly creating  probabilistic databases (see \secref{sec:query}).
Next, our $\Omega$--View builder uses these inferred probability density functions to create a probabilistic database, as shown in the \texttt{prob\_view} table of \figref{fig:framework}.

\vspace{0.2cm}
Suppose that the data values of a probabilistic database are decomposed into a set of ranges $\Omega =\{\omega_1,\omega_2,\cdots,\omega_n\}$, where $\omega_i=[\omega_i^l,\omega_i^u]$ is bounded by a lower bound $\omega_i^l$ and an upper bound $\omega_i^u$. Then, the $\Omega$--View builder corresponds to the following query in order to compute probability values for the given ranges:

%\begin{definition} \label{def:prob-esti-query}{\bf Probability value generation query.}
%    Given a probability density function $p_t(R_t)$ and a set of ranges $\Omega=\{\omega_1,\omega_2,\cdots,\omega_n\}$ for the probability values in a probabilistic database, a probability value generation query returns a set of probabilities $\Lambda_t =\{\rho_{\omega_1}, \rho_{\omega_2}, \cdots, \rho_{\omega_n}\}$ at time $t$, where $\rho_{\omega_i}$ is the probability of occurrence of $\omega_i \in \Omega$ and is equal to $\int_{\omega_i^l}^{\omega_i^u} {p_t(R_t) dR_t}$.
%\end{definition}

\vspace{0.1cm}
%Recall the example shown in \figref{fig:intro-example}. Let us assume that $\omega_1$ corresponds to the event of Alice being present in \linebreak \emph{Room 1}. At time $t=1$, Alice is likely to be in \emph{Room 1} (i.e., $\omega_1$ occurs) with probability $\rho_{\omega_1}=0.5$.
%
%Note that the creation of probabilistic databases can be performed in either online or offline fashion. In the online mode, the dynamic density metrics infer $p_t(R_t)$ as soon as a new value $r_t$ is streamed to the system. In the offline mode, users may give SQL-like queries to the system (examples are provided in \secref{sec:query}).

\begin{table}[!hbt]
  \small
  \centering
  \caption{Summary of Notations}
  \label{tab:notation}
  %\setlength{\extrarowheight}{2pt}
  \begin{tabular}{cl}
  \hline
  \textbf{Symbol}       & \textbf{Description} \\ \hline
   $S$                  & A time series. \\
   $S^H_{t-1}$          & Sliding window having $H$ values $[t-H, t-1]$.\\
   $r_t$                & Raw (imprecise) value at time $t$. \\
   $R_t$                & Random variable associated with $r_t$. \\
   $\hat{r}_t,\bb{E}(R_t)$ & Expected true value at time $t$. \\
   $p_t(R_t)$           & Probability density function of $R_t$ at time $t$. \\
   $P_t(R_t)$           & Cumulative probability distribution function of $R_t$ \\
                       & at time $t$. \\
   $\rho_\omega$               & Probability of occurrence of event $\omega$. \\
   $\bb{E}(X)$          & Expected value of random variable $X$. \\
%   $\bb{E}(X|Y)$        & conditional expectation of random variable $X$ \\
%                        &  given random variable $Y$  \\ \hline
   $\mathcal{N}(\mu,\sigma^2)$     & Normal (Gaussian) probability density function\\
                         & with mean $\mu$ and variance $\sigma^2$. \\
   $\Omega$     & A set of ranges for creating probability values \\
                & in a probabilistic database. \\
   $\lceil x \rceil$     & A smallest integer value that is not smaller than $x$. \\
%           $cdf$         & cumulative distribution function \\ \hline
%           $pdf$         & probability density function \\ \hline
\hline
  \end{tabular}
%\vspace{-0.3cm}

\end{table}

\subsection{Evaluation of Dynamic Density Models}

Quantifying the quality of a dynamic density metric is crucial, since it reflects the quality of a probabilistic database created. Here, we introduce an effective measure, termed \emph{density distance}, that quantifies the quality of a probability density inferred by a dynamic density metric.

%In addition, the \emph{density distance} is a generic measure of quality which is completely independent of the models used for producing probabilistic data.

Let $p_t(R_t)$ be an inferred probability density at time $t$. A straightforward manner in which we can evaluate the quality of this inference is to compare $p_t(R_t)$ with its corresponding true density $\hat{p}_t(R_t)$. $\hat{p}_t(R_t)$, however, cannot be given nor observed, rendering this straightforward evaluation infeasible.
%As a result, our task of evaluating the accuracy of the dynamic density metrics becomes even more challenging.
%
To overcome this, we propose to use an indirect method for evaluating the quality of a dynamic density metric known as the \emph{probability integral transform} \cite{Diebold1998}.
%This method gives a consistent measure of accuracy, while overcoming the problem of unobservable $\hat{p}_i(R_i)$.
%Particularly, we use the definition given by Diebold~\etal~\cite{Diebold1998} for measuring the accuracy of time-dependent density inference. The definition uses a mathematical technique known as \emph{probability integral transform}.
A probability integral transform of a random variable $X$, with probability density function $f(X)$, transforms $X$ to a uniformly distributed random variable $Y$ by evaluating $Y= \int_{-\infty}^x f(X=u)du$ where $x \in X$. Thus, the probability integral transform of $r_i$ with respect to $p_i(R_i)$ becomes, $z_i = \int_{-\infty}^{r_i} p_i(R_i = u) du.$

Let $p_1(R_1),\ldots, p_t(R_t)$ be a sequence of probability distributions inferred using a dynamic density metric. Also, let $z_1,\ldots,z_t$ be the probability integral transforms of raw values $r_1,\ldots,r_t$ with respect to $p_1(R_1),\ldots, p_t(R_t)$. Then, $z_1,\ldots,z_t$ are uniformly distributed between $(0,1)$ if and only if the inferred probability density $p_i(R_i)$ is equal to the true density $\hat{p}_i(R_i)$ for $i=1,2,\ldots,t$ \cite{Diebold1998}.

%Then Diebold~\etal~\cite{Diebold1998} showed that $z_1,\ldots,z_N$ are uniformly distributed between $(0,1)$ when $p_t(R_t) = \hat{p}_t(R_t)$.
%
To find out whether $z_1,\ldots,z_t$ follow a uniform distribution we estimate the cumulative distribution function of $z_1,\ldots,z_t$ using a histogram approximation method. Let us denote this cumulative distribution function as $Q_{Z}(z)$. We define the quality measure of a dynamic density metric as the Euclidean distance between $Q_{Z}(z)$ and the ideal uniform cumulative distribution function between $(0,1)$ denoted as $U_{Z}(z)$. Formally, the quality measure is defined as:

%% \begin{displaymath}
%%   \sqrt{\int_{x=0}^{1} (U_{Z_t}(x) - Q_{Z_t}(x))^2}.
%% \end{displaymath}
%
%% But, in practice we evaluate the above integral as a summation since $Q_{Z_t}$ is available at only discrete values,
\begin{equation}
\label{eqn:accy-measure}
  d\{U_{Z}(z),Q_{Z}(z)\} = \sqrt{\sum_{x=0}^{1} (U_{Z}(x) - Q_{Z}(x))^2}.
\end{equation}
We refer to $d\{U_{Z}(z),Q_{Z}(z)\}$ as \emph{density distance}. The density distance quantifies the difference between the observed distribution of $z_1,\ldots,z_t$ and their expected distribution. Thus, it gives a measure of quality for the inferred densities $p_1(R_1),\ldots, p_t(R_t)$. The density distance will be used in \secref{sec:exp} to compare the effectiveness of each dynamic density metrics this paper introduces.

%Higher the density distance lower is the accuracy of that particular dynamic density metric. Thus, we can conveniently use density distance to compare the accuracy of various dynamic density metrics.

%% As an example, \figref{fig:vol-mo-comp}($a$) shows $Q_{Z_t}(z_t)$ obtained from a sample output of the cumulative distribution functions obtained by evaluating the probability integral transform at $r_t$ for the various fidelity metrics discussed in \secref{sec:metrics}. Now suppose the cumulative distribution function of the transformed variable obtained using uniform thresholding metrics be $F_1(z_t)$, then we use $F_1(z_t)$ and $U_{z_t}(z_t)$ in \eqnref{eqn:accy-measure} to
%% obtain the accuracy measure $d\{U_{Z_t}(z_t),F_{1}(z_t)\}$.

%Next, we also define the concept of \emph{expected true value}:
%
%\begin{definition}{\bf Expected true value}\\
%     Given a set of parameters $E_t$ for a probability density function $p_t(R_t)$, the expected true value $\hat{r_t}$ is the expected value of $R_t$.
%    \label{def:true-value}
%\end{definition}

%Next, we formally define a fidelity database as follow:
%%
%\begin{definition}(Fidelity Database)
%    A fidelity database~\linebreak $F=\langle (r_1,E_1), (r_2,E_2), \cdots, (r_t,E_t) \rangle$ is a sequence of pairwise tuples $(r_i,E_i)$, where $r_i$ and $E_i$ denote a raw value and a set of fidelity elements at time $i$, respectively.
%\end{definition}

%
%\begin{definition}
%Probabilistic Nearest Neighbor (PNN) Query: Given a
%query point q and a set of objects with uncertain attributes and their corre-
%sponding pdfs, a PNN query returns the probability Pnn(O) that object O is NN
%to q for each object O.
%\end{definition}
%
%\begin{definition}
%Probabilistic Nearest Neighbor Threshold (PNNT) Query:
%Given a query point q, a threshold and a set of objects with uncertain attributes
%and their pdfs, the PNNT query returns every object O with Pnn(O) > .
%\end{definition}
%

