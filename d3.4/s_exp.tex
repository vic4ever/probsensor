\chapter{Experimental Evaluation} \label{sec:exp}
The main goals of our experimental study are fourfold. First, we show that the performance of the proposed dynamic density
metrics, namely, \armagarch~and \kalgarch~are efficient and accurate over real-world data. Second, we compare the performance of
the \armagarch~metric with that of the \ourgarch~enhancement, in order to show that \ourgarch~is efficient as well as accurate in handling erroneous
values in time series. We then demonstrate that the use of the $\sigma$--cache significantly increases query processing
performance. Lastly, we perform experiments validating that real world datasets exhibit regimes of changing volatility.

In our experiments, we use two real datasets, details of these datasets are as follows:

\sstitle{Campus Dataset} This dataset comprises of ambient temperature values recorded over twenty five days. It consists of
approximately eighteen thousand samples. These values are obtained from a real sensor network deployment on the EPFL university campus in Lausanne, Switzerland.
We refer to this dataset as \emph{campus-data}.

\sstitle{Moving Object Dataset} This dataset consists of GPS logs recorded from on-board navigation systems in 192 cars in
Copenhagen, Denmark. Each log entry consists of time and x-y coordinate values. In our evaluation
we use only x-coordinate values. This dataset contains approximately ten thousand samples recorded over five and half hours.  We
refer to this dataset as \emph{car-data}.

%In our observation \emph{car-data} is smoother than \emph{campus-data}. This is natural since a moving car follows a
%predefined path of roads and freeways which contain smooth change of direction.
\tabref{tab:dataset-summary} provides a summary
of important properties of both datasets. We have implemented all our methods using MATLAB Ver. 7.9 and Java Ver. 6.0. We use a
Intel Dual Core 2 GHz machine having 3GB of main memory for performing the experiments.

%% \section{Datasets and Experimental Setup}
%% The first dataset is obtained from a sensor network while the second
%% dataset consists of GPS values obtained for a moving car. In our observation the GPS data is inherently more smooth than
%% sensor network data, since moving cars follow a predefined path of roads and freeways which exhibit only smooth
%% change of direction. \tabref{tab:dataset-summary} provides a summary of important properties of both datasets.
%% %

%% \sstitle{Campus Dataset} This dataset consists of ambient temperature values obtained from a sensor deployment monitoring various
%% atmospheric parameters on a university campus. For our experimental evaluation we use ambient temperature values for sixty five
%% hours (approx. two thousand samples).  We refer to this dataset as \emph{campus-data}.

%% \sstitle{Moving Object Dataset} This dataset consists of GPS logs of the trajectory traced by a car. Each log entry consists of time,
%% x-coordinate, and y-coordinate. In our experimental evaluation we only use x-coordinate values. Here we
%% use sixty five minutes (approx. two thousand samples) for experimental evaluation. We refer to this data as \emph{car-data}.

\begin{table}[!h]
\small
\centering
\caption{Summary of Datasets}
\label{tab:dataset-summary}
\begin{tabular}{|c|c|c|}
\hline
         & \emph{campus-data} & \emph{car-data}  \\ \hline
%         & Dataset & Object Dataset \\ \hline
 Monitored parameter & Temperature & GPS Position \\
 Number of data values & 18031 & 10473 \\
 Sensor accuracy & $\pm$ 0.3 deg. C & $\pm$ 10 meters \\
 Sampling interval & 2 minutes & 1-2 seconds\\ \hline
\end{tabular}
\end{table}
%

\vspace{-0.4cm}
\section{Comparison of Dynamic Density Metrics}
\label{subsec:ddm-comp}
We compare our main proposals (\armagarch~and \kalgarch) with uniform thresholding (UT) and variable thresholding (VT). These
evaluations are performed on both datasets. As described in \secref{sec:prob}, we used the density distance for comparing the quality of distributions obtained using the dynamic density metrics.

%We use the sensor accuracy (see \tabref{tab:dataset-summary}) for computing the uniform
%thresholding metric. Let $S=\langle r_1, r_2, \ldots,$ $ r_{t_m} \rangle$ be the raw time series. Then we evaluate all the dynamic
%density metrics on sliding windows $S^H_{t-1}$ where $H \leq t \leq t_{m}$ by computing the density distance (see
%\secref{sec:prob}) for each metric.
%

\figref{fig:ts-vs-dist} shows a comparison of density distance for the various dynamic density metrics for both datasets along with
increasing window size ($H$).
%During this experimental evaluation we use an ARMA(2,0) model.
Clearly, both the \armagarch~metric and
the \kalgarch~metric outperform the naive density metrics. Specifically, those advanced dynamic density metrics outperform the naive density metrics by giving \emph{upto 20 times and 12.3 times lower} density distances for \emph{campus-data} and \emph{car-data} respectively.
%\fixme{should be remove this senstence?} For the \armagarch
%metric we observe that when the window size increases the density distance initially decreases and then roughly remains
%constant.

\begin{figure}[!h]
    \centering
    \includegraphics[width=1.0\columnwidth]{gp-fig/big-dataset-train-size-vs-vol-distance}
    \vspace{-0.6cm}
    \caption{Comparing quality of the dynamic density metrics.}
    \label{fig:ts-vs-dist}
\end{figure}

Among the advanced dynamic density metrics, the \armagarch~metric performs better than all the other metrics. For \emph{car-data}
we can observe that the \kalgarch~metric gives low accuracy as the window size increases. This behavior is expected since when
larger window sizes are used for the Kalman Filter, there is a greater chance of error in inferring $\hat{r}_t$. In our
observation, the use of smaller window sizes (e.g., $H=10$) for the \kalgarch~metric performs twice better, compared to the
\armagarch~metric.

%% Given that the density distance is low for \armagarch~metric and \kalgarch~metric
%% which assume that the data follows a gaussian distribution we can say that the raw values also follow a gaussian distri that
%% whether the real conditional density is close to the assumed conditional density. Thus the small distance between \garch~model, which
%% assumes conditional Gaussianity, suggests that the underlying data also follows a conditional Gaussian distribution.


%Moreover, even though the \kalgarch~metric performs better on lower window size it is inefficient.
Next, we compare the efficiency of the dynamic density metrics. \figref{fig:ts-vs-time}
shows the average times required to perform one iteration of density inference. Because of the large performance
gain of the \armagarch~metric, the execution times are shown on logarithmic scale. The \armagarch~metric achieves a \emph{factor
  of 5.1 to 18.6 speedup} over the \kalgarch~metric.  This is due to slow convergence of the iterative EM
(Expectation-Maximization) algorithm used for estimating parameters of the Kalman Filter. Thus, unlike the \arma~model, computing
parameters for the Kalman Filter takes longer for large window sizes.  The naive dynamic density metrics are much more
efficient than the \kalgarch~metric. But they are only marginally better than the \armagarch~metric.
Overall the \armagarch~metric shows excellent characteristics in terms of both efficiency and \nolinebreak accuracy.
\begin{figure}[!b]
    \centering
    \includegraphics[width=1.0\columnwidth]{gp-fig/big-dataset-train-size-vs-lgs-time}
    \vspace{-0.6cm}
    \caption{Comparing efficiency of the dynamic density metrics. Note the logarithmic scale on the y-axis.}
    \label{fig:ts-vs-time}
\end{figure}

%
%% Also,
%% \figref{fig:vol-mo-comp}($a$) compares the accuracy of volatility prediction for Kalman Filter and \arma~model for
%% increasing training window size. Observe that for small window size Kalman Filter give good volatility prediction accuracy as
%% compared to \arma~model. But, from \figref{fig:time-comp}($a$) we see that Kalman Filter are less efficient as compared to \arma~model.
%% Apart from being efficient, one of the primary drawbacks of \arma~model is that they cannot, unlike Kalman Filter, handle
%% observation noise which makes their use infeasible for hidden value inference \cite{shumway2005}. Thus between Kalman Filter and
%% \arma~model we get a well understood efficiency versus accuracy trade-off.
%% \begin{figure}
%% \centering
%% \subfloat[]{ \includegraphics[width=0.48\columnwidth]{fig/vol_accuracy_comp.pdf}}  \hfil
%% %\subfloat[]{\includegraphics[width=0.48\columnwidth]{fig/arch_test.pdf}} \hfil
%% \qquad
%% \caption{(a) Sample output of the Diebold method for comparing accuracy of density prediction. (b) Testing for heteroscedasticity in the \emph{campus-data}.}
%% \label{fig:vol-mo-comp}
%% \end{figure}

In the next set of experiments, we discuss the effect of model order of an ARMA(p,0) model on density
distance. \figref{fig:mo-comp} shows the density distance obtained by using several metrics when the model order $p$ increases.
Observe that for the \armagarch~metric the density distance increases with model order. This justifies our choice of a low model
order for the \armagarch~metric.

\begin{figure}[!t]
    \center
    \includegraphics[width=0.5\columnwidth]{gp-fig/model-order-vs-vol-distance}
    \caption{Effect of model order on \emph{campus-data}.}
    \label{fig:mo-comp}
\end{figure}
%% \stitle{Scaling behavior of GARCH model}
%% Here we show that the \garch~model exhibits linear scaling \emph{w.r.t.} the window size. To show this we record the time taken by the \garch~model for various window sizes of the \emph{campus-data}. The result from this experiment is shown in \figref{fig:time-comp}($b$). Observe that the time taken for inference using \garch~model increases linearly \wrt~the window size. This behavior distinctly establishes the scalability of \garch~model.
%

\vspace{-0.15cm}
\section{Impact of C-GARCH}
In the following, we demonstrate the improved performance of the \ourgarch~model by comparing it with the plain \armagarch~metric using \emph{campus-data} (we omit the results from \emph{car-data} because they are similar).
%on the basis of efficiency and accuracy of detecting erroneous values. Recall, that the \ourgarch~enhancement was developed in \secref{sec:xgarch} since the \garch~model shows drastically different behavior in presence of erroneous values. Moreover, given the well understood fact that real world data contains erroneous values we developed \ourgarch~for online cleaning of erroneous values.
We start by inserting erroneous values synthetically, since for comparing accuracy we should know beforehand the number of erroneous values present in the data. The insertion procedure inserts a pre-specified number of very high (or very low) values uniformly at random in the data.

For evaluating the \ourgarch~approach we first compute $SV_{max}$ using a given set of clean values and then execute the \ourgarch~model
while setting $o_{c_{max}} = 8$. \figref{fig:roofnet-cgarch}($a$) compares the percentage of total erroneous values detected for
\ourgarch~and \armagarch. Admittedly, the \ourgarch~approach is \emph{more than twice effective} in detecting and cleaning erroneous
values. Additionally, from \figref{fig:roofnet-cgarch}($b$) it can be observed that the \ourgarch~approach does not require
excessive computational cost as compared to \armagarch.
\begin{figure}[!h]
    \centering
    \includegraphics[width=1.0\columnwidth]{gp-fig/roofnet-cgarch-results}
    \vspace{-0.6cm}
    \caption{Comparing \ourgarch~and~\garch. (a) Percentage of erroneous values successfully detected and (b) average time for processing a single value.}
    \label{fig:roofnet-cgarch}
\end{figure}
The reason is that the \arma~model estimation takes more time if there
are erroneous values in the window $S^H_{t-1}$. This additional time offsets the time spent by the \ourgarch~model in cleaning
erroneous values before they are given to the \armagarch~metric.

%% We set $o_{c_{max}} = 8$, this value is the only additional input given to \ourgarch~as compared to \armagarch~metric. Then we use the \emph{CleanGARCH} algorithm (see \algoref{alg:ourgarch}) for performing online cleaning of erroneous values while setting $\kappa = 3$ thus making the probability of not finding $r_t$ between $u_b$ and $l_b$ low. Next, we record the number of erroneous values detected in the data and report their percentage. For comparison with \armagarch~we give the same data to the \armagarch~metric and report the percentage of erroneous values detected. \figref{fig:roofnet-cgarch}.($a$) shows this comparison \emph{w.r.t} the number of erroneous values inserted. Clearly, the \ourgarch~approach is more than twice effective in detecting and cleaning erroneous values. To verify that \ourgarch~does not incur any additional time as compared to \armagarch~metric, in \figref{fig:roofnet-cgarch}.($a$) we compare the average time required to process one sample. It can be observed that the \ourgarch~model takes less time as compared to \armagarch~metric although the \ourgarch~model uses the \armagarch~metric. This happens because the \armagarch~metric takes longer time if there are erroneous values in the estimation window. This offests the additional time spent by the \ourgarch~model for detecting and cleaning erroneous values.

%% is the case since as the \garch~model does not clean erroneous values effectively there are erroneous values present in the training data. This results in more time for estimation as compared to \ourgarch~which has most of the erroneous values from training data successfully cleaned.

%\section{Impact of using {\Large $\sigma$}--cache}
%Next, we show the impact of using the $\sigma$--cache while creating a probabilistic database. Particularly, we are interested in
%knowing the increase in efficiency obtained from using a $\sigma$--cache. Moreover, we are also interested in verifying the rate at
%which the size of the $\sigma$--cache grows as the maximum ratio threshold $\mathcal{D}_s$ increases. Here, we
%expect the cache size to grow logarithmically in $\mathcal{D}_s$.
%
%We use \emph{campus-data} for demonstrating the space and time efficiency of the $\sigma$--cache. We choose $\Delta=0.05$,
%$n=300$, Hellinger distance $\mathcal{H}=0.01$, and compute $d_s$ using
%\eqnref{eqn:error-gar}. \figref{fig:sigma-index-impact}($a$) shows the improvement in efficiency obtained for the probabilistic
%view generation query with increasing number of tuples. Here, the naive approach signifies that the
%$\sigma$--cache is not used for storing and reusing previous computation.
%%\footnote{Recall, each tuple in the table of raw values stores parameters of distribution $p_t(R_t)$. See \figref{fig:framework}.}.
%%
%\begin{figure}[!h]
%\centering
%\subfloat[]{ \includegraphics[width=0.486\columnwidth]{gp-fig/db-size-vs-time.pdf}}  \hfil
%\subfloat[]{ \includegraphics[width=0.486\columnwidth]{gp-fig/maxrt-vs-memory.pdf}}  \hfil
%\qquad
%\vspace{-0.3cm}
%\caption{(a) Impact of using the $\sigma$--cache on efficiency. (b) Scaling behavior of the $\sigma$--cache. Note the exponential scale on the x-axis.}
%\label{fig:sigma-index-impact}
%\end{figure}
%%
%In \figref{fig:sigma-index-impact} all values are computed by taking an ensemble average over ten independent
%executions. Clearly, using the $\sigma$--cache exhibits manyfold improvements in efficiency. For example, when there are 18K raw
%value tuples we observe a \emph{factor of 9.6 speedup} over the naive approach. \figref{fig:sigma-index-impact}($b$) shows the
%memory consumed by the $\sigma$--cache as $\mathcal{D}_s$ is increased. As expected, the cache size grows only logarithmically as
%the maximum ratio threshold $\mathcal{D}_s$ increases. This proves that the $\sigma$-cache is a space- and time-efficient method for seamlessly
%caching and reusing computation.
%


%\begin{table} [h!]
%  \centering
%  \small
%  \begin{tabular}[b]{|c|c|c|c|}
%   \hline Approach              & Thresholding  &  Particle Filter  & GARCH\\
%   \hline fidelity accuracy     &  low          &  high             & high  \\
%   \hline efficiency            &  high         &  low              & high \\
%   \hline prob. distribution    &  uniform      &  any              & Gaussian  \\
%   \hline parameter for what-if &  1            &  many             & 2  \\
%   \hline
%  \end{tabular}
% \caption{Comparison of Fidelity Metrics} \label{tab:metrics} \fixme{complete this}
%\end{table}

%% \begin{table}[!hbt]
%%   \centering
%%   \small
%%   \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
%%   \hline
%%   \small
%%      $m$ & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\ \hline % & 9 & 10 \\ \hline
%%      $\Phi(m)$ & 10.1 & 13.7 & 17 & 21 & 24.2 & 26.1 & 27.6 & 29.2 \\ \hline % & 30.8 & 32.4 \\ \hline
%%      $\chi^2(m)$ &  2.7 & 4.6 & 6.3 & 7.8 & 9.2 & 10.6 & 12 & 13.3 \\ \hline %    14.6837 15.9872
%%      $H_0$ & Rej. & Rej. & Rej. & Rej. & Rej. & Rej. & Rej. & Rej. \\ \hline
%%   \end{tabular}
%% \caption{Testing for the ARCH effect in our dataset} \label{tab:arch-test}
%% \end{table}

\section{Verifying Time-varying Volatility}
\label{subsec:verify-tvol}
Before we infer time-varying volatility using the \armagarch~metric or the \kalgarch~metric it is important to verify whether a
given time series exhibits changes in volatility over time. For testing this we use a null
hypothesis test proposed in \cite{shumway2005}. The null hypothesis tests whether the errors obtained from using a \arma~model
($a^2_i$) are independent and identically distributed (\emph{i.i.d}). This is equivalent to testing whether $\xi_1=\cdots=\xi_m=0$
in the linear regression,
%
\begin{equation}
\label{eqn:nullhyp-reg}
a^2_i = \xi_0 + \xi_1 a^2_{i-1} + \cdots + \xi_m a^2_{i-m} + e_i,
\end{equation}
where $i \in \{m+1,\ldots, H\}$, $e_i$ denotes the error term, $m \geq 1$, and $H$ is the window size. If we reject the null
hypothesis (i.e., $\xi_j \neq 0$) then we can say that the errors are not \emph{i.i.d}, thus establishing that the
given time series exhibits time-varying volatility. First, we start by computing the sample variance of $a^2_i$ and $e_i$ denoted as $\gamma_0$ and $\gamma_1$ respectively. Then,
%
%let $\gamma_1 = \sum_{i=m+1}^K e^2_i$ where $e_i$ are the residuals of the linear regression in \eqnref{eqn:nullhyp-reg}. Then,
%
%First, we start by computing the sample mean of $a^2_i$ as $\mu(a^2_i) = \frac{1}{K}\sum_{i=1}^K a^2_i$. Also, let $\gamma_0 =
%\sum^K_{i=m+1} (a^2_i - \mu(a^2_i))^2$ and $\gamma_1 = \sum_{i=m+1}^K e^2_i$ where $e_i$ are the residuals of the linear
%regression in \eqnref{eqn:nullhyp-reg}. Then,
%
\begin{equation}
\Phi(m) = \frac{(\gamma_0-\gamma_1)/m}{\gamma_1 / (K-2m-1)},
\end{equation}
is asymptotically distributed as a chi-square distribution $\chi^2_m$ with $m$ degrees of freedom. Thus we reject the null
hypothesis if $\Phi(m) > \chi^2_{m}(\alpha)$, where $\chi^2_{m}(\alpha)$ is in the upper $100(1-\alpha)^{th}$ percentile of
$\chi^2_m$ or the p-value of $\Phi(m) < \alpha$ \cite{shumway2005}. In our experiments we choose $\alpha = 0.05$.

%
To show that our datasets exhibit regimes of changing volatility we compute the value of $\Phi(m)$  where $m = \{1,2, \ldots, 8 \}$ on 1800 windows containing 180 samples each (i.e., $H=180$) for \emph{campus-data} and \emph{car-data}. Then we reject the null hypothesis if the average value of $\Phi(m)$ over all windows is greater than $\chi^2_{m}(\alpha)$.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.95\columnwidth]{gp-fig/garch-effect}
    \caption{Verifying time-varying volatility.}
    \label{fig:arch-test}
\end{figure}

\figref{fig:arch-test} shows the results from this evaluation. Clearly, we can reject the null
hypothesis for both datasets because for all values of $m$, $\chi^2_{m}(\alpha)$ is much lower than
$\Phi(m)$. This means that $a^2_i$ are not \emph{i.i.d} and thus we can find regimes of changing volatility. Interestingly,
for \emph{car-data} (see \figref{fig:arch-test}($b$)) we can see that $\chi^2_{m}(\alpha)$ and $\Phi(m)$ are close to each other.
Thus the \emph{car-data} contains less time-varying volatility as compared to the \emph{campus-data}.

The above results support the claim that real datasets show change of volatility with time, thus justifying the use of the
\garch~model.

%However, the implication of these experiments is not limited to our datasets since this clearly contradicts the assumption of constant volatility made by previous works \cite{cheng03,Cheng2004}.



%% \begin{figure}[!h]
%% \centering
%% %\subfloat[]{ \includegraphics[width=0.486\columnwidth]{fig/model-order-vs-distance.pdf}}  \hfil % old figures
%% %\subfloat[]{\includegraphics[width=0.486\columnwidth]{fig/garch-scaling-with-time.pdf}} \hfil % old figures
%% \subfloat[]{ \includegraphics[width=0.486\columnwidth]{gp-fig/model-order-vs-vol-distance.pdf}}  \hfil
%% \subfloat[]{ \includegraphics[width=0.486\columnwidth]{gp-fig/garch-scale-char.pdf}}  \hfil
%% \qquad
%% \caption{(a) Effect of model order. (b) Scaling behavior of \garch~model.}
%% \label{fig:time-comp}
%% \end{figure}

\section{Experiment with multi-sensor data}
