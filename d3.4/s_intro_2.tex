\chapter{Introduction}
\label{sec:intro}

One of the most effective ways to deal with imprecise and uncertain data is to employ probabilistic approaches. In recent years
there have been a plethora of methods for managing and querying uncertain data
\cite{cheng03,Hua2008,Dalvi2007,Olteanu2009,Tao2005,Cormode2007,re08}. These methods are typically based on the
assumption that probabilistic data used for processing queries is available; however, this is not always true. \emph{Creating} probabilistic
data is a challenging and still unresolved problem. Prior work on this problem has only limited scope for domain-specific
applications, such as handling duplicated tuples \cite{Andritsos2006,Hassanzadeh2009} and deriving structured data from
unstructured data \cite{Gupta2006}. Evidently, a wide range of applications still lack the benefits of existing query processing
techniques that require probabilistic data. Time-series data is one important example where probabilistic data processing is
currently not widely applicable due to the lack of probability values. Although, the benefits are evident given that time series,
in particular generated from sensors (environmental sensors, RFID, GPS, etc.), are often imprecise and uncertain in nature.

Before diving into the details of our approach let us consider a motivating example (see \figref{fig:intro-example}).  Alice is
tracked by indoor-positioning sensors and her locations are recorded in a database table called \texttt{raw\_values} in the form
of a three-tuple $\langle time,x,y\rangle$. These raw values are generally imprecise and uncertain due to several noise factors
involved in position measurement, such as low-cost sensors, discharged batteries, and network failures. On the other hand,
consider a probabilistic query where an application is interested in knowing, given a particular time, the probability that Alice
could be found in each of the four rooms. For answering this query we need the table \texttt{prob\_view} (see
\figref{fig:intro-example}). This table gives us the probability of finding Alice in a particular room at a given time. To derive
the \texttt{prob\_view} table from the \texttt{raw\_values} table, however, the system faces a fundamental problem---how to
\emph{meaningfully} associate a probability distribution $p(R)$ with each raw value tuple $\langle time,x,y\rangle$, where $R$ is
the random variable associated with Alice's position.

\begin{figure}[b]
    \center
    \includegraphics[width=\columnwidth]{fig/intro-example}
    \caption{An example of creating a tuple-level probabilistic database from time-dependent probability distributions.}
    \label{fig:intro-example}
\end{figure}

Once the system associates a probability distribution $p(R)$ with each tuple,  it can be used to derive probabilistic views, which forms
a probabilistic database used for evaluating various types of probabilistic queries \cite{cheng03,Dalvi2007}. Thus, this example clearly illustrates the importance of having
a means for creating probabilistic databases. Nevertheless, there is a lack of effective tools that are capable of creating such probabilistic
databases. In an effort to rectify this situation, we focus on the problem of creating a probabilistic
database from given (imprecise) time series, thereupon, facilitating direct processing of a variety of probabilistic queries.

%% One important challenge in creating such a probabilistic database from time series is to deal with evolving probability
%% distributions since time series often exhibit highly irregular dependencies on time. For example, temperature changes dramatically
%% around sunrise and sunset, but changes only slightly during the night. This implies that the probability distributions that are
%% used as the basis for deriving probabilistic databases also change over time and thus must be computed dynamically. Thus from the
%% time-series literature we identify and adopt a novel class of dynamical models for modeling time-dependent probability
%% distributions. Furthermore, we extend these models to handle erroneous inputs. The time-dependent probability distributions
%% obtained from these models are then used for creating probabilistic databases.
%The
%time-series literature offers a wide variety of mathematical models for modeling time-dependent probability distributions. Hence,
%from this comprehensive collection of models we carefully identify a novel class of dynamical models for creating tuple-level
%probabilistic databases (or data streams) from imprecise time-series data.

Unfortunately, creating probabilistic databases from imprecise time-series data poses several important challenges. In the following
paragraphs we elaborate these challenges and discuss the solutions that this paper proposes.

\stitle{Inferring Evolving Probability Distributions}
One of the most important challenges in creating a probabilistic database
from time series is to deal with evolving probability distributions, since time series often exhibit highly irregular dependencies
on time \cite{Cormode2007,Tran2009}. For example, temperature changes dramatically around sunrise and sunset, but changes only
slightly during the night. This implies that the probability distributions that are used as the basis for deriving probabilistic
databases also change over time, and thus must be computed dynamically.

In order to capture the evolving probability distributions of time series we introduce various \emph{dynamic density metrics},
each of them dynamically infers time-dependent probability distributions from a given time series. The distributions derived by these dynamic density
metrics are then used for creating probabilistic databases. After carefully analyzing several dynamical models for representing the dynamic density metrics (details are provided in \secref{sec:metrics} and \secref{sec:exp}), we identify and adopt a novel class of dynamical models from the time-series literature, which is known as the \garch~(\underline{G}eneralized
\underline{A}uto\underline{R}egressive \underline{C}onditional \underline{H}eteroskedasticity) model~\cite{shumway2005}.
%Our main proposal among these metrics identifies and adopts a novel
%class of dynamical models from the time-series literature, which is known as the \garch~(\underline{G}eneralized
%\underline{A}uto\underline{R}egressive \underline{C}onditional \underline{H}eteroskedasticity) model~\cite{shumway2005}.
We show
that the \garch~model can play an important role in efficiently and accurately creating probabilistic databases, by effectively
inferring dynamic probability distributions.

%% Thus from the
%% time-series literature we identify and adopt a novel class of dynamical models for modeling time-dependent probability
%% distributions. Furthermore, we extend these models to handle erroneous inputs. The time-dependent probability distributions
%% obtained from these models are then used for creating probabilistic databases.
%% In order to capture the evolving probability distributions of time series we introduce various {\em dynamic density metrics}, each
%% of which dynamically infers time-dependent probability distributions.  The distributions produced by these dynamic density metrics
%% are then used for creating probabilistic databases.  Our main proposal among these metrics adopts the \garch~(Generalized
%% AutoRegressive Conditional Heteroskedasticity) model~\cite{shumway2005} that has been widely used for forecasting volatility of
%% time series. Here, we show that the \garch~model can also play an important role in accurately creating probabilistic databases,
%% by effectively inferring dynamic probability distributions.

An important challenge in identifying appropriate dynamic density metrics is to find a measure that precisely assess the quality of the
probability distributions produced by these metrics.
%Once we have identified appropriate dynamic density metrics, a subsequent challenge is to fairly assess the quality of probability
%distributions produced by these metrics.
This assessment is important since it quantifies the quality of probabilistic  databases
derived using these probability distributions. A straightforward method is to compare the ground truth (i.e., true probability
distributions) with the inference obtained from our dynamic density metrics, thus producing a tangible measure of
quality. This is, however, infeasible since we can neither observe the ground truth nor establish it unequivocally by
any other means. To circumvent this crucial limitation, we propose an indirect method for measuring quality, termed \emph{density
  distance}, which is based on a solid mathematical framework. The density distance is a generic measure of quality, which is
independent of the models used for producing probabilistic databases.

Unfortunately, the \garch~model works inappropriately on time series that contain erroneous values, i.e., significant outliers,
which are often produced by sensors. This is because the \garch~model is generally used over precise, certain, and clean data
(e.g., stock market data). In contrast, the time series that this study considers are typically imprecise and erroneous. Thus, we
propose an improved version of the \garch~model, termed \ourgarch, that performs appropriately in the presence of such erroneous
values.

%In contrast, time series that this study considers are typically imprecise and erroneous.  To cope
%with this, we propose an improved version of the \garch~model, termed \ourgarch, that performs appropriately in the presence of
%such erroneous values.

%\pagebreak
%\stitle{Efficiently Creating Probabilistic Databases}
%Given probability distributions inferred by a dynamic density metric, the next step of our solution is to generate views that
%contain probability values (e.g., \texttt{prob\_view} in \figref{fig:intro-example}). We introduce the
%\emph{$\Omega$-View builder} that efficiently creates probabilistic views by processing a {\em probability value generation
%  query}. The output of this query can be directly consumed by a wide variety of existing probabilistic queries, thus enabling
%higher level probabilistic reasoning.
%
%%% All the dynamic density metrics we identify are parametric in nature. This choice is dictated by the efficient storage and
%%% retrieval properties exhibited by the parametric models. But as discussed in the example in \figref{fig:intro-example},
%%% probabilistic queries are generic and are not designed to operate on a particular class of models. To bridge this gap, we propose
%%% a new query type, which we call the \emph{probabilistic value generation} query. The output of this query can be directly consumed
%%% by a large number of existing probabilistic queries, thus further enabling higher level reasoning. Overall, we propose a query
%%% provisioning layer called the \emph{$\Omega$--View builder}, this layer provides transparent support for creating probabilistic
%%% databases. Particularly, it takes the probabilistic value generation query as an input and efficiently \emph{builds} probabilistic
%%% databases.
%
%Since the probabilistic value generation query accepts arbitrary time intervals (past or current) as inputs, this could incur
%heavy computational overhead on the system when the time interval spans over a large number of raw values. To address this, we
%present an effective caching mechanism called \emph{$\sigma$-cache}. The $\sigma$--cache caches and reuses probability values
%computed at previous times for current time processing. We experimentally demonstrate that the $\sigma$--cache boosts the
%efficiency of query processing by an \emph{order of magnitude}. Additionally, we provide theoretical guarantees that are used for
%setting the cache parameters. These guarantees enable the choice of the cache parameters under user-defined constraints of storage
%space and error tolerance. Moreover, such guarantees make the $\sigma$--cache an attractive solution for large-scale data processing.

\stitle{Contributions}
%This paper provides a complete solution for creating probabilistic databases.
To the best of our knowledge, this is the first work that offers a generic end-to-end solution for creating
probabilistic databases from arbitrary imprecise time-series data. Specifically, we first introduce various dynamic density metrics
for associating tuples of raw values with probability distributions. Since sensors often deliver error prone data values we
propose effective enhancements which make the dynamic density metrics robust against unclean data.
%Since raw values that are delivered by the sensors are prone to errors, we strengthen our methods to be resilient against
% erroneous inputs.
We then suggest approaches which allow applications to efficiently create probabilistic databases by using a
SQL-like syntax.

To summarize, this paper makes the following contributions:
\begin{itemize}
\item \setlength{\parskip}{-1pt} We adopt a novel class of models for proposing various dynamic density metrics. We then enhance
  these metrics by improving their resilience against erroneous inputs.

\item \setlength{\parskip}{-1pt} We introduce density distance that quantifies the effectiveness of the dynamic density
  metrics. This serves as an important measure for indicating the quality of probabilistic databases derived using a dynamic
  density \nolinebreak metric.

%% \item \setlength{\parskip}{-1pt} Since sensors rarely deliver ``clean'' data we propose effective enhancements which makes these
%%   dynamic density metrics robust against erroneous values.

\item \setlength{\parskip}{-1pt} We present a generic framework comprising of a malleable query provisioning layer (i.e., $\Omega$--View builder) which allows us to create
  probabilistic databases with minimal effort.

\item \setlength{\parskip}{-1pt} We propose space- and time-efficient caching mechanisms
  (i.e., $\sigma$--cache) which produce manyfold improvement in performance. Furthermore, we prove useful guarantees for effectively setting the cache parameters.

\item \setlength{\parskip}{-1pt} We extensively evaluate our methods by performing experiments on two real datasets.
\end{itemize}

 We begin by giving details of our framework for generating probabilistic databases in
\secref{sec:prob}. \secref{sec:metrics} introduces the naive dynamic density metrics while in \secref{sec:garch} we propose the
\garch~metric. An enhancement of the \garch~metric, \ourgarch,~is discussed in
\secref{sec:xgarch}. In \secref{sec:query}, we suggest effective methods for generating probabilistic databases, this is followed
by a discussion on $\sigma$--cache. Lastly, \secref{sec:exp} presents comprehensive experimental evaluations followed by the review of related studies in \secref{sec:related}.
